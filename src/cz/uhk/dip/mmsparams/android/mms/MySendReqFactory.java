package cz.uhk.dip.mmsparams.android.mms;

import android.content.Context;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import com.android.mms.dom.smil.parser.SmilXmlSerializer;
import com.google.android.mms.ContentType;
import com.google.android.mms.InvalidHeaderValueException;
import com.google.android.mms.pdu_alt.CharacterSets;
import com.google.android.mms.pdu_alt.EncodedStringValue;
import com.google.android.mms.pdu_alt.MySendReq;
import com.google.android.mms.pdu_alt.PduBody;
import com.google.android.mms.pdu_alt.PduPart;
import com.google.android.mms.smil.SmilHelper;
import com.klinker.android.send_message.Utils;

import org.thoughtcrime.securesms.logging.Log;
import org.thoughtcrime.securesms.util.Util;

import java.io.ByteArrayOutputStream;

import cz.uhk.dip.mmsparams.android.pdus.PduByteHelper;
import cz.uhk.dip.mmsparams.api.utils.PduConverters;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsAttachmentSendModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsSendModel;

public class MySendReqFactory
{
    private static final String TAG = MySendReqFactory.class.getSimpleName();

    public static MySendReq constructSendPdu(Context context, MmsSendModel mmsSend)
    {
        MySendReq req = new MySendReq();

        String lineNumber = Utils.getMyPhoneNumber(context);


        if (!TextUtils.isEmpty(lineNumber))
        {
            if (mmsSend.isSenderVisible())
                req.setFrom(new EncodedStringValue(lineNumber));
        }
        else
        {
            req.setFrom(new EncodedStringValue(""));
        }

        if (mmsSend.getTo() != null)
        {
            for (String number : mmsSend.getTo())
            {
                req.addTo(new EncodedStringValue(StringUtil.removeSpaces(number)));
            }
        }

        if (mmsSend.getBcc() != null)
        {
            for (String number : mmsSend.getBcc())
            {
                req.addBcc(new EncodedStringValue(StringUtil.removeSpaces(number)));
            }
        }

        if (mmsSend.getCc() != null)
        {
            for (String number : mmsSend.getCc())
            {
                req.addCc(new EncodedStringValue(StringUtil.removeSpaces(number)));
            }
        }


        req.setDate(System.currentTimeMillis() / 1000);

        PduBody body = new PduBody();
        int size = 0;


        if (!TextUtils.isEmpty(mmsSend.getText()))
        {
            PduPart part = new PduPart();
            String name = String.valueOf(System.currentTimeMillis());
            part.setData(Util.toUtf8Bytes(mmsSend.getText()));
            part.setCharset(CharacterSets.UTF_8);
            part.setContentType(ContentType.TEXT_PLAIN.getBytes());
            part.setContentId(name.getBytes());
            part.setContentLocation((name + ".txt").getBytes());
            part.setName((name + ".txt").getBytes());

            body.addPart(part);
            size += PduByteHelper.getPartSize(part);
        }
        // Add attachments

        if (mmsSend.getMmsAttachmentSendModel() != null)
        {
            for (MmsAttachmentSendModel attachment : mmsSend.getMmsAttachmentSendModel())
            {
                try
                {
//                    if (attachment.getDataUri() == null)
//                        throw new IOException("Assertion failed, attachment for outgoing MMS has no data!");

                    String fileName = attachment.getFileName();
                    PduPart part = new PduPart();


                    if (fileName == null)
                    {
                        fileName = String.valueOf(Math.abs(Util.getSecureRandom().nextLong()));
                        String fileExtension = MimeTypeMap.getSingleton().getExtensionFromMimeType(attachment.getMimeType());

                        if (fileExtension != null) fileName = fileName + "." + fileExtension;
                    }

                    if (attachment.getMimeType().startsWith("text"))
                    {
                        part.setCharset(CharacterSets.UTF_8);
                    }

                    part.setContentType(attachment.getMimeType().getBytes());
                    part.setContentLocation(fileName.getBytes());
                    part.setName(fileName.getBytes());

                    int index = fileName.lastIndexOf(".");
                    String contentId = (index == -1) ? fileName : fileName.substring(0, index);
                    part.setContentId(contentId.getBytes());
                    //part.setData(Util.readFully(PartAuthority.getAttachmentStream(context, attachment.getDataUri())));
                    part.setData(attachment.getData());

                    body.addPart(part);
                    size += PduByteHelper.getPartSize(part);
                }
                catch (Exception e)
                {
                    Log.w(TAG, e);
                }
            }
        }


        ByteArrayOutputStream out = new ByteArrayOutputStream();
        SmilXmlSerializer.serialize(SmilHelper.createSmilDocument(body), out);
        PduPart smilPart = new PduPart();
        smilPart.setContentId("smil".getBytes());
        smilPart.setContentLocation("smil.xml".getBytes());
        smilPart.setContentType(ContentType.APP_SMIL.getBytes());
        smilPart.setData(out.toByteArray());
        body.addPart(0, smilPart);

        req.setBody(body);
        req.setMessageSize(size);


        req.setMessageClass(PduConverters.getClassValue(mmsSend.getMsgClass()));
        req.setExpiry(mmsSend.getExpiry()); // 7 * 24 * 60 * 60


        try
        {
            //req.setDrmContent(profile.getDrmContentVal());
            //req.setAdaptationAllowed(profile.getAdaptationAllowedVal());

            req.setPriority(PduConverters.getPriority(mmsSend.getPriority()));
            req.setDeliveryReport(PduConverters.getYesNoValue(mmsSend.isDeliveryReport()));
            req.setReadReport(PduConverters.getYesNoValue(mmsSend.isReadReport()));
            req.setDeliveryTime(mmsSend.getDeliveryTime());
            req.setSenderIsVisible(mmsSend.isSenderVisible());
        }
        catch (InvalidHeaderValueException e)
        {
            Log.e(TAG, "Set Pr, DR, RR, RR, DT, SiV", e);
        }


        return req;
    }

}
