package cz.uhk.dip.mmsparams.android.helpers;

import android.content.Context;

import androidx.annotation.NonNull;
import cz.uhk.dip.mmsparams.android.pdus.PduHeaderConvertor;
import cz.uhk.dip.mmsparams.android.preferences.MmsParamsPreferences;

public class MessageSizeHelper
{
    public static boolean allowLargeMms(@NonNull Context context)
    {
        String value = MmsParamsPreferences.getMaxAttachSize(context);

        if (PduHeaderConvertor.ATTACH_SIZE_UNLIMITED.equals(value))
        {
            return true;
        }

        return false;
    }
}

