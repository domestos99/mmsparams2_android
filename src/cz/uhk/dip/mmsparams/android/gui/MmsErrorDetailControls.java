package cz.uhk.dip.mmsparams.android.gui;

import android.view.View;
import android.widget.TextView;

import cz.uhk.dip.mmsparams.android.R;
import org.thoughtcrime.securesms.logging.Log;
import org.thoughtcrime.securesms.util.ViewUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.fragment.app.FragmentManager;
import cz.uhk.dip.mmsparams.android.db.model.ErrorBLO;
import cz.uhk.dip.mmsparams.android.db.model.MmsError;
import cz.uhk.dip.mmsparams.android.gui.base.BaseFragment;
import cz.uhk.dip.mmsparams.api.json.JsonUtils;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;

public class MmsErrorDetailControls extends DetailControlsBase<MmsError>
{
    public static final String TAG = MmsErrorDetailControls.class.getSimpleName();


    TextView tvTag, tvDateTime, tvInfoMsg, tvExMessage, tvStackTrace;


    protected MmsErrorDetailControls(BaseFragment baseFragment, FragmentManager fragmentManager, View view)
    {
        super(baseFragment, fragmentManager, view);
    }


    @Override
    protected void initControls(View view)
    {
        tvTag = ViewUtil.findById(view, R.id.tvTag);
        tvDateTime = ViewUtil.findById(view, R.id.tvDateTime);
        tvInfoMsg = ViewUtil.findById(view, R.id.tvInfoMsg);
        tvExMessage = ViewUtil.findById(view, R.id.tvExMessage);
        tvStackTrace = ViewUtil.findById(view, R.id.tvStackTrace);
    }

    @Override
    protected void setupListeners()
    {

    }

    @Override
    protected MmsError updateModelFromControls(MmsError obj)
    {
        return obj;
    }

    @Override
    protected void fillControls(MmsError obj)
    {
        tvTag.setText("my tag");


        long ms = obj.getDateCreated();
        if (ms < 0)
            tvDateTime.setText(String.valueOf(ms));

        Date dt = new Date(ms);
        String dateTimeS = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss").format(dt);
        tvDateTime.setText(dateTimeS);


        String json = obj.getError();
        if (StringUtil.isEmptyOrNull(json))
            return;

        try
        {
            ErrorBLO blo = JsonUtils.fromJson(json, ErrorBLO.class);

            tvTag.setText(blo.tag);
            tvExMessage.setText(blo.message);
            tvInfoMsg.setText(blo.info);
            tvStackTrace.setText(blo.stackTrace);
        }
        catch (Exception e)
        {
            Log.e(TAG, "setupView", e);
        }


    }

}

