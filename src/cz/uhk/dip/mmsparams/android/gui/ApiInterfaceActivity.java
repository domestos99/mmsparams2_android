package cz.uhk.dip.mmsparams.android.gui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import cz.uhk.dip.mmsparams.android.gui.base.BaseFragment;
import cz.uhk.dip.mmsparams.android.gui.base.MyListPassphraseRequiredActionBarActivity;
import cz.uhk.dip.mmsparams.android.gui.base.OnFragmentInteractionListener;

public class ApiInterfaceActivity extends MyListPassphraseRequiredActionBarActivity implements OnFragmentInteractionListener
{

    BaseFragment fragment;


    @Override
    protected BaseFragment getListFragment()
    {
        fragment = new ApiInterfaceFragment();
        return fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState, boolean ready)
    {
        super.onCreate(savedInstanceState, ready);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    @Override
    public void onFragmentInteraction(Uri uri)
    {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = getSupportFragmentManager().findFragmentById(android.R.id.content);
        fragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected boolean beforeActivityFinish()
    {
        boolean close = fragment.handleOnClosing();

        if (close)
        {
            onBackPressed();
        }
        return true;

    }
}

