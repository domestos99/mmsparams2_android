package cz.uhk.dip.mmsparams.android.gui.base;

import android.net.Uri;

public interface OnFragmentInteractionListener
{
    void onFragmentInteraction(Uri uri);
}
