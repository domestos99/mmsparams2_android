package cz.uhk.dip.mmsparams.android.gui.base;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;

public abstract class BaseFragment extends BaseFragmentLight
{


    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(GetLayoutResource(), container, false);

        beforeBaseInit();

        baseInit(view);

        afterBaseInit();

        return view;
    }

    protected void beforeBaseInit()
    {
    }

    protected void afterBaseInit()
    {
    }


    protected abstract String getMyTag();


    protected final void baseInit(final View view)
    {
        initControls(view);
        setupListeners();
    }

    protected abstract
    @LayoutRes
    int GetLayoutResource();

    protected abstract void initControls(final View view);

    protected abstract void setupListeners();

    public final boolean handleOnClosing()
    {
        BaseFragment bf = super.getActiveFragment();

        if (bf == null)
            return true;
        else
        {
            return bf.handleBeforeClose();
            // return bf.handleOnClosing();
        }
    }

    protected boolean handleBeforeClose()
    {
        return true;
    }


}

