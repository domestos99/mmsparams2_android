package cz.uhk.dip.mmsparams.android.gui.base;

import android.content.Context;
import android.view.View;

import androidx.fragment.app.FragmentManager;

public abstract class ControlsBase
{

    protected final BaseFragment baseFragment;
    protected final Context context;
    protected final FragmentManager fragmentManager;
    protected final View view;

    protected ControlsBase(BaseFragment baseFragment, FragmentManager fragmentManager, View view)
    {
        this.baseFragment = baseFragment;
        this.context = baseFragment.getApplicationContext();
        this.fragmentManager = fragmentManager;
        this.view = view;

        beforeBaseInit();
        baseInit(view);
        afterBaseInit();
    }

    protected void beforeBaseInit()
    {
    }

    protected void afterBaseInit()
    {
    }


    protected final void baseInit(final View view)
    {
        initControls(view);
        setupListeners();
    }

    protected abstract void initControls(final View view);

    protected abstract void setupListeners();
}
