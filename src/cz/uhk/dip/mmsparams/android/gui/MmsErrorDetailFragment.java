package cz.uhk.dip.mmsparams.android.gui;

import android.view.View;

import cz.uhk.dip.mmsparams.android.R;
import org.thoughtcrime.securesms.database.DatabaseFactory;

import cz.uhk.dip.mmsparams.android.db.MmsErrorDatabase;
import cz.uhk.dip.mmsparams.android.db.model.MmsError;
import cz.uhk.dip.mmsparams.android.gui.base.DetailFragment;

public class MmsErrorDetailFragment extends DetailFragment<MmsError>
{
    public static final String TAG = MmsErrorDetailFragment.class.getSimpleName();

    MmsErrorDetailControls controls;


    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.mms_error_detail_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        controls = new MmsErrorDetailControls(this, getFragmentManager(), view);
    }

    @Override
    protected void setupListeners()
    {

    }


    @Override
    protected MmsError getById(int id)
    {
        MmsErrorDatabase db = DatabaseFactory.getMmsErrorDatabase(getContext());
        return db.getById(id);
    }

    @Override
    protected MmsError getNew()
    {
        return null;
    }

    @Override
    protected void saveData(MmsError instance)
    {

    }

    @Override
    protected void deleteData(int id)
    {

    }

    @Override
    protected void fillControls(MmsError obj)
    {
        controls.fillControls(obj);
    }

    @Override
    protected MmsError updateModel(MmsError obj)
    {
        return controls.updateModelFromControls(obj);
    }
}
