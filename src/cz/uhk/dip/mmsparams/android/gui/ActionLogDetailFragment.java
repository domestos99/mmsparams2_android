package cz.uhk.dip.mmsparams.android.gui;

import android.view.View;

import cz.uhk.dip.mmsparams.android.R;
import org.thoughtcrime.securesms.database.DatabaseFactory;

import cz.uhk.dip.mmsparams.android.db.ActionLogDatabase;
import cz.uhk.dip.mmsparams.android.db.model.ActionLogModel;
import cz.uhk.dip.mmsparams.android.gui.base.DetailFragment;

public class ActionLogDetailFragment extends DetailFragment<ActionLogModel>
{

    public static final String TAG = ActionLogDetailFragment.class.getSimpleName();

    private ActionLogDetailDetailControls controls;


    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.action_log_detail_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        controls = new ActionLogDetailDetailControls(this, getFragmentManager(), view);

        setHasOptionsMenu(true);
    }


    @Override
    protected void setupListeners()
    {

    }

    @Override
    protected void afterLoadData()
    {
        super.afterLoadData();
        controls.afterLoadData(ID);
    }


    @Override
    protected ActionLogModel getById(int id)
    {
        return getDB().getById(id);
    }

    @Override
    protected ActionLogModel getNew()
    {
        return null;
    }

    @Override
    protected void saveData(ActionLogModel instance)
    {

    }

    @Override
    protected void deleteData(int id)
    {

    }

    @Override
    protected void fillControls(ActionLogModel obj)
    {
        controls.fillControls(obj);
    }

    @Override
    protected ActionLogModel updateModel(ActionLogModel obj)
    {
        return controls.updateModelFromControls(obj);
    }

    private ActionLogDatabase getDB()
    {
        return DatabaseFactory.getActionLogDatabase(getApplicationContext());
    }

}

