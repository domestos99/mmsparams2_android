package cz.uhk.dip.mmsparams.android.gui;

import android.view.View;

import androidx.fragment.app.FragmentManager;
import cz.uhk.dip.mmsparams.android.gui.base.BaseFragment;
import cz.uhk.dip.mmsparams.android.gui.base.ControlsBase;

public abstract class DetailControlsBase<T> extends ControlsBase
{

    protected DetailControlsBase(BaseFragment baseFragment, FragmentManager fragmentManager, View view)
    {
        super(baseFragment, fragmentManager, view);
    }

    protected abstract T updateModelFromControls(T obj);

    protected abstract void fillControls(T obj);


}
