package cz.uhk.dip.mmsparams.android.gui;

import android.view.View;
import android.widget.Button;

import cz.uhk.dip.mmsparams.android.R;
import org.thoughtcrime.securesms.util.ViewUtil;

import androidx.fragment.app.FragmentManager;
import cz.uhk.dip.mmsparams.android.gui.base.BaseFragment;
import cz.uhk.dip.mmsparams.android.gui.base.ControlsBase;
import cz.uhk.dip.mmsparams.android.permissions.PermissionUtil;

public class PermissionViewControls extends ControlsBase
{
    protected PermissionViewControls(BaseFragment baseFragment, FragmentManager fragmentManager, View view)
    {
        super(baseFragment, fragmentManager, view);
    }

    private Button btnAskFormPermissions;


    @Override
    protected void initControls(View view)
    {
        btnAskFormPermissions = ViewUtil.findById(view, R.id.btnAskFormPermissions);

        updateControls();
    }

    @Override
    protected void setupListeners()
    {
        btnAskFormPermissions.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                askForPerm();
            }
        });

    }

    private void askForPerm()
    {
        PermissionUtil.checkAllPerm(baseFragment.getActivity());
    }

    public void updateControls()
    {
        if (PermissionUtil.hasAllPerm(baseFragment.getActivity()))
        {
            btnAskFormPermissions.setEnabled(false);
            btnAskFormPermissions.setBackgroundColor(baseFragment.getResources().getColor(R.color.green_500));
        }
        else
        {
            btnAskFormPermissions.setEnabled(true);
            btnAskFormPermissions.setBackgroundColor(baseFragment.getResources().getColor(R.color.red_400));
        }
    }
}

