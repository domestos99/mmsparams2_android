package cz.uhk.dip.mmsparams.android.gui;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;

import org.thoughtcrime.securesms.PassphraseRequiredActionBarActivity;
import org.thoughtcrime.securesms.database.DatabaseFactory;
import org.thoughtcrime.securesms.util.DynamicLanguage;
import org.thoughtcrime.securesms.util.DynamicTheme;

import androidx.annotation.NonNull;
import cz.uhk.dip.mmsparams.android.db.MessagePduDatabase;
import cz.uhk.dip.mmsparams.android.db.model.MessagePdu;
import cz.uhk.dip.mmsparams.android.gui.base.OnFragmentInteractionListener;

public class MessagePduDetailActivity extends PassphraseRequiredActionBarActivity implements OnFragmentInteractionListener
{
    public final static String MESSAGE_ID_EXTRA = "message_id";
    public final static String ID_EXTRA = "id";
    private static final String TAG = MessagePduDetailActivity.class.getSimpleName();


    private DynamicTheme dynamicTheme = new DynamicTheme();
    private DynamicLanguage dynamicLanguage = new DynamicLanguage();

    @Override
    protected void onPreCreate()
    {
        dynamicTheme.onCreate(this);
    }


    public static MessagePdu getGenericPdu(Context context, int id)
    {
        MessagePduDatabase db = DatabaseFactory.getmessagePduDatabase(context);
        MessagePdu pdu = db.getById(id);


        if (pdu == null)
            return null;

        return pdu;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState, @NonNull boolean ready)
    {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        long messageId = getIntent().getLongExtra(MESSAGE_ID_EXTRA, -1);
        Bundle bundle = new Bundle();
        bundle.putLong(MESSAGE_ID_EXTRA, messageId);
        initFragment(android.R.id.content, new MessagePduDetailListFragment(), dynamicLanguage.getCurrentLocale(), bundle);
    }

    @Override
    public void onResume()
    {
        dynamicTheme.onResume(this);
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);

        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
        }

        return false;
    }

    @Override
    public void onFragmentInteraction(Uri uri)
    {

    }


}
