package cz.uhk.dip.mmsparams.android.gui.pdu;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.mms.pdu_alt.MyNotificationInd;
import com.google.android.mms.pdu_alt.ReadOrigInd;

import cz.uhk.dip.mmsparams.android.R;
import org.thoughtcrime.securesms.util.ViewUtil;

import cz.uhk.dip.mmsparams.android.db.model.MessagePdu;
import cz.uhk.dip.mmsparams.android.gui.MessagePduDetailActivity;
import cz.uhk.dip.mmsparams.android.pdus.PduHeaderConvertor;

public class MessagePduReadOrigIndFragment extends MessagePduBaseFragment
{
    private int paramID;
    private ReadOrigInd readOrigInd;
    private TextView tvMessageFrom, tvMessageType, tvMmsVersion, tvMmsTo, tvMmsMessageID, tvMmsDate, tvMmsReadStatus, tvPduCreated;
    private MessagePdu messagePdu;
    public static final String TAG = MessagePduReadOrigIndFragment.class.getSimpleName();

    public static MessagePduReadOrigIndFragment newInstance(int paramID)
    {
        MessagePduReadOrigIndFragment fragment = new MessagePduReadOrigIndFragment();
        Bundle args = new Bundle();
        args.putInt(MessagePduDetailActivity.ID_EXTRA, paramID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void beforeBaseInit()
    {
        if (getArguments() != null)
        {
            paramID = getArguments().getInt(MessagePduDetailActivity.ID_EXTRA);
            messagePdu = MessagePduDetailActivity.getGenericPdu(getContext(), paramID);
            readOrigInd = (ReadOrigInd) messagePdu.getGenericPdu();
        }
    }

    @Override
    protected String getMyTag()
    {
        return TAG;
    }

    @Override
    protected int GetLayoutResource()
    {
        return R.layout.message_pdu_read_orig_ind_fragment;
    }

    @Override
    protected void initControls(View view)
    {
        tvPduCreated = ViewUtil.findById(view, R.id.tvPduCreated);

        tvMessageFrom = ViewUtil.findById(view, R.id.tvMessageFrom);
        tvMessageType = ViewUtil.findById(view, R.id.tvMessageType);
        tvMmsVersion = ViewUtil.findById(view, R.id.tvMmsVersion);


        tvMmsTo = ViewUtil.findById(view, R.id.tvMmsTo);
        tvMmsMessageID = ViewUtil.findById(view, R.id.tvMmsMessageID);
        tvMmsDate = ViewUtil.findById(view, R.id.tvMmsDate);
        tvMmsReadStatus = ViewUtil.findById(view, R.id.tvMmsReadStatus);


        fillControls();
    }

    @Override
    protected void setupListeners()
    {

    }

    private void fillControls()
    {
        try
        {
            tvPduCreated.setText(getCreatedString(messagePdu.getDateCreated()));

            tvMessageFrom.setText(getNumberString(readOrigInd.getFrom()));
            tvMessageType.setText(PduHeaderConvertor.getMessageTypeString(readOrigInd.getMessageType()));
            tvMmsVersion.setText(PduHeaderConvertor.getMmsVersionString(readOrigInd.getMmsVersion()));

            tvMmsTo.setText(getToAsString(readOrigInd.getTo()));
            tvMmsMessageID.setText(getString(readOrigInd.getMessageId()));
            tvMmsDate.setText(getDateTime(readOrigInd.getDate()));
            tvMmsReadStatus.setText(PduHeaderConvertor.getReadStatusString(readOrigInd.getReadStatus()));


        }
        catch (Exception e)
        {
            int k = 0;
        }
    }


}
