package cz.uhk.dip.mmsparams.android.facades;

import android.content.Context;

import cz.uhk.dip.mmsparams.api.json.JsonUtilsSafe;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public class WebSocketLoggerFacade
{
    private static final String TAG = WebSocketLoggerFacade.class.getSimpleName();

    public static void logIncomingMessage(final Context context, String message)
    {
        ActionLogLoggerFacade.log(context, TAG, "Incomming Message", message);
        LoggerFacade.log(TAG, "Incomming Message: " + message);
    }

    public static void logOutgoingMessage(final Context context, String message)
    {
        ActionLogLoggerFacade.log(context, TAG, "Outcomming Message", message);
        LoggerFacade.log(TAG, "Outcomming Message: " + message);
    }

    public static void logUnknownMessage(final Context context, String message)
    {
        ActionLogLoggerFacade.log(context, TAG, "Unknown Message", message);
        LoggerFacade.log(TAG, "Unknown Message: " + message);
    }

    public static void logOutgoingMessage(final Context context, String tag, WebSocketMessageBase message, boolean isResponse)
    {
        String json = JsonUtilsSafe.toJson(message);
        ActionLogLoggerFacade.log(context, tag, "Outgoing; isResponse:" + isResponse, json);
        LoggerFacade.log(tag, "Outgoing: " + json + " ;;; isResponse:" + isResponse);
    }
}
