package cz.uhk.dip.mmsparams.android.facades;

import android.content.Context;

import org.thoughtcrime.securesms.database.DatabaseFactory;
import org.thoughtcrime.securesms.logging.Log;

import cz.uhk.dip.mmsparams.android.db.ActionLogDatabase;
import cz.uhk.dip.mmsparams.android.db.model.ActionLogModel;

public class ActionLogLoggerFacade
{
    private static final String TAG = ActionLogLoggerFacade.class.getSimpleName();

    public static void log(final Context context, String tag, String msg, String body)
    {
        try
        {
            ActionLogModel model = new ActionLogModel();
            model.setTag(tag);
            model.setMessage(msg);
            model.setBody(body);

            ActionLogDatabase db = DatabaseFactory.getActionLogDatabase(context);
            db.insert(model);
        }
        catch (Exception ex)
        {
            Log.e(TAG, "log", ex);
        }
    }

}
