package cz.uhk.dip.mmsparams.android.pdus;

import android.telephony.SmsMessage;

import com.google.android.mms.pdu_alt.MyAcknowledgeInd;
import com.google.android.mms.pdu_alt.MyDeliveryInd;
import com.google.android.mms.pdu_alt.MyNotificationInd;
import com.google.android.mms.pdu_alt.MyNotifyRespInd;
import com.google.android.mms.pdu_alt.MyReadOrigInd;
import com.google.android.mms.pdu_alt.MyReadRecInd;
import com.google.android.mms.pdu_alt.MyRetrieveConf;
import com.google.android.mms.pdu_alt.MySendConf;
import com.google.android.mms.pdu_alt.MySendReq;
import com.google.android.mms.pdu_alt.PduBody;
import com.google.android.mms.pdu_alt.PduPart;

import java.util.ArrayList;

import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.AcknowledgeIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.DeliveryIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotificationIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.NotifyRespIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.PduPartModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.ReadOrigIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.ReadRecIndModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.RetrieveConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendConfModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.pdus.SendReqModel;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsDeliveryReport;
import cz.uhk.dip.mmsparams.api.websocket.model.sms.SmsReceiveModel;

public class PduModelConvertor
{
    public static AcknowledgeIndModel GetModel(MyAcknowledgeInd pdu)
    {
        AcknowledgeIndModel Model = new AcknowledgeIndModel();

        Model.setFrom(ConvertUtils.getFrom(pdu.getFrom()));
        Model.setMmsVersion(ConvertUtils.getMmsVersion(pdu.getMmsVersion()));
        Model.setReportAllowed(ConvertUtils.getReportAllowed(pdu.getReportAllowed()));
        Model.setTransactionId(ConvertUtils.getTransactionId(pdu.getTransactionId()));

        return Model;
    }

    public static DeliveryIndModel GetModel(MyDeliveryInd pdu)
    {
        DeliveryIndModel Model = new DeliveryIndModel();

        Model.setMmsVersion(ConvertUtils.getMmsVersion(pdu.getMmsVersion()));
        Model.setFrom(ConvertUtils.getFrom(pdu.getFrom()));
        Model.setDate(ConvertUtils.getDate(pdu.getDate()));
        Model.setMessageId(ConvertUtils.getMessageId(pdu.getMessageId()));
        Model.setStatus(ConvertUtils.getStatus(pdu.getStatus()));
        Model.setTo(ConvertUtils.getTo(pdu.getTo()));

        Model.setApplicId(ConvertUtils.getApplicId(pdu.getApplicId()));
        Model.setAuxApplicId(ConvertUtils.getAuxApplicId(pdu.getAuxApplicId()));
        Model.setReplyApplicId(ConvertUtils.getReplyApplicId(pdu.getReplyApplicId()));
        Model.setStatusText(ConvertUtils.getStatusText(pdu.getStatusText()));


        return Model;
    }

    public static NotificationIndModel GetModel(MyNotificationInd pdu)
    {
        NotificationIndModel Model = new NotificationIndModel();

        Model.setContentClass(ConvertUtils.getContentClass(pdu.getContentClass()));
        Model.setContentLocation(ConvertUtils.getContentLocation(pdu.getContentLocation()));
        Model.setExpiry(ConvertUtils.getExpiry(pdu.getExpiry()));
        Model.setMessageClass(ConvertUtils.getMessageClass(pdu.getMessageClass()));

        Model.setDeliveryReport(ConvertUtils.getDeliveryReport(pdu.getDeliveryReport()));
        Model.setTransactionId(ConvertUtils.getTransactionId(pdu.getTransactionId()));
        Model.setSubject(ConvertUtils.getSubject(pdu.getSubject()));
        Model.setMmsVersion(ConvertUtils.getMmsVersion(pdu.getMmsVersion()));
        Model.setFrom(ConvertUtils.getFrom(pdu.getFrom()));


        Model.setApplicId(ConvertUtils.getApplicId(pdu.getApplicId()));
        Model.setAuxApplicId(ConvertUtils.getAuxApplicId(pdu.getAuxApplicId()));
        Model.setDistributionIndicator(ConvertUtils.getDistributionIndicator(pdu.getDistributionIndicator()));
        Model.setPriority(ConvertUtils.getPriority(pdu.getPriority()));
        Model.setRecommendedRetrievalMode(ConvertUtils.getRecommendedRetrievalMode(pdu.getRecommendedRetrievalMode()));
        Model.setRecommendedRetrievalModeText(ConvertUtils.getRecommendedRetrievalModeText(pdu.getRecommendedRetrievalModeText()));
        Model.setReplaceId(ConvertUtils.getReplaceId(pdu.getReplaceId()));
        Model.setReplyCharging(ConvertUtils.getReplyCharging(pdu.getReplyCharging()));
        Model.setReplyChargingDeadline(ConvertUtils.getReplyChargingDeadline(pdu.getReplyChargingDeadline()));
        Model.setReplyChargingId(ConvertUtils.getReplyChargingId(pdu.getReplyChargingId()));
        Model.setReplyChargingSize(ConvertUtils.getReplyChargingSize(pdu.getReplyChargingSize()));
        Model.setStored(ConvertUtils.getStored(pdu.getStored()));

        // TODO
        // Model.setMessageSize(ConvertUtils.getMessageSize(pdu.getMessageSize()));

        return Model;
    }

    public static NotifyRespIndModel GetModel(MyNotifyRespInd pdu)
    {
        NotifyRespIndModel Model = new NotifyRespIndModel();

        Model.setReportAllowed(ConvertUtils.getReportAllowed(pdu.getReportAllowed()));
        Model.setStatus(ConvertUtils.getStatus(pdu.getStatus()));
        Model.setTransactionId(ConvertUtils.getTransactionId(pdu.getTransactionId()));
        Model.setMmsVersion(ConvertUtils.getMmsVersion(pdu.getMmsVersion()));
        Model.setFrom(ConvertUtils.getFrom(pdu.getFrom()));

        return Model;
    }

    public static PduPartModel[] GetPduBodyModel(PduBody pduBody)
    {
        if (pduBody == null)
            return null;

        int count = pduBody.getPartsNum();

        PduPartModel[] a = new PduPartModel[count];
        for (int i = 0; i < count; i++)
        {
            a[i] = GetModel(pduBody.getPart(i));
        }
        return a;
    }

    public static PduPartModel GetModel(PduPart pduPart)
    {
        PduPartModel Model = new PduPartModel();

        Model.setContentId(ConvertUtils.getContentId(pduPart.getContentId()));
        Model.setContentType(ConvertUtils.getContentType(pduPart.getContentType()));
        Model.setContentLocation(ConvertUtils.getContentLocation(pduPart.getContentLocation()));
        Model.setName(ConvertUtils.getName(pduPart.getName()));
        Model.setFileName(ConvertUtils.getFilename(pduPart.getFilename()));
        Model.setCharSet(ConvertUtils.getCharset(pduPart.getCharset()));
        Model.setContentTransferEncoding(ConvertUtils.getContentTransferEncoding(pduPart.getContentTransferEncoding()));
        Model.setData(ConvertUtils.getData(pduPart.getData()));
        Model.setDataUri(ConvertUtils.getDataUri(pduPart.getDataUri()));
        Model.setContentDisposition(ConvertUtils.getContentDisposition(pduPart.getContentDisposition()));
        Model.setDataLength(ConvertUtils.getDataLength(pduPart.getDataLength()));
        Model.setGenerateLocation(ConvertUtils.generateLocation(pduPart.generateLocation()));

        return Model;
    }

    public static ReadOrigIndModel GetModel(MyReadOrigInd pdu)
    {
        ReadOrigIndModel Model = new ReadOrigIndModel();

        Model.setDate(ConvertUtils.getDate(pdu.getDate()));
        Model.setMessageId(ConvertUtils.getMessageId(pdu.getMessageId()));
        Model.setTo(ConvertUtils.getTo(pdu.getTo()));
        Model.setReadStatus(ConvertUtils.getReadStatus(pdu.getReadStatus()));
        Model.setMmsVersion(ConvertUtils.getMmsVersion(pdu.getMmsVersion()));
        Model.setFrom(ConvertUtils.getFrom(pdu.getFrom()));


        Model.setApplicId(ConvertUtils.getApplicId(pdu.getApplicId()));
        Model.setAuxApplicId(ConvertUtils.getAuxApplicId(pdu.getAuxApplicId()));
        Model.setReplyApplicId(ConvertUtils.getReplyApplicId(pdu.getReplyApplicId()));


        return Model;
    }

    public static ReadRecIndModel GetModel(MyReadRecInd pdu)
    {
        ReadRecIndModel Model = new ReadRecIndModel();

        Model.setDate(ConvertUtils.getDate(pdu.getDate()));
        Model.setReadStatus(ConvertUtils.getReadStatus(pdu.getReadStatus()));
        Model.setTo(ConvertUtils.getTo(pdu.getTo()));
        Model.setMessageId(ConvertUtils.getMessageId(pdu.getMessageId()));
        Model.setMmsVersion(ConvertUtils.getMmsVersion(pdu.getMmsVersion()));
        Model.setFrom(ConvertUtils.getFrom(pdu.getFrom()));

        return Model;
    }

    public static RetrieveConfModel GetModel(MyRetrieveConf pdu)
    {
        RetrieveConfModel Model = new RetrieveConfModel();

        Model.setContentType(ConvertUtils.getContentType(pdu.getContentType()));
        Model.setDeliveryReport(ConvertUtils.getDeliveryReport(pdu.getDeliveryReport()));
        Model.setMessageClass(ConvertUtils.getMessageClass(pdu.getMessageClass()));
        Model.setMessageId(ConvertUtils.getMessageId(pdu.getMessageId()));
        Model.setTransactionId(ConvertUtils.getTransactionId(pdu.getTransactionId()));
        Model.setCc(ConvertUtils.getCc(pdu.getCc()));
        Model.setReadReport(ConvertUtils.getReadReport(pdu.getReadReport()));
        Model.setRetrieveStatus(ConvertUtils.getRetrieveStatus(pdu.getRetrieveStatus()));
        Model.setRetrieveText(ConvertUtils.getRetrieveText(pdu.getRetrieveText()));
        Model.setMmsVersion(ConvertUtils.getMmsVersion(pdu.getMmsVersion()));
        Model.setFrom(ConvertUtils.getFrom(pdu.getFrom()));
        Model.setTo(ConvertUtils.getTo(pdu.getTo()));
        Model.setDate(ConvertUtils.getDate(pdu.getDate()));
        Model.setSubject(ConvertUtils.getSubject(pdu.getSubject()));
        Model.setPriority(ConvertUtils.getPriority(pdu.getPriority()));

        Model.setParts(GetPduBodyModel(pdu.getBody()));

        Model.setApplicId(ConvertUtils.getApplicId(pdu.getApplicId()));
        Model.setAuxApplicId(ConvertUtils.getAuxApplicId(pdu.getAuxApplicId()));
        Model.setContentClass(ConvertUtils.getContentClass(pdu.getContentClass()));
        Model.setDrmContent(ConvertUtils.getDrmContent(pdu.getDrmContent()));
        Model.setDistributionIndicator(ConvertUtils.getDistributionIndicator(pdu.getDistributionIndicator()));
//        Model.setPreviouslySentBy(pdu.getPreviouslySentBy());
//        Model.setPreviouslySentDate(pdu.getPreviouslySentDate());
//        Model.setMmFlags(pdu.getMmFlags());
//        Model.setMmState(pdu.getMmState());
        Model.setReplaceId(ConvertUtils.getReplaceId(pdu.getReplaceId()));
        Model.setReplyApplicId(ConvertUtils.getReplyApplicId(pdu.getReplyApplicId()));
        Model.setReplyCharging(ConvertUtils.getReplyCharging(pdu.getReplyCharging()));
        Model.setReplyChargingDeadline(ConvertUtils.getReplyChargingDeadline(pdu.getReplyChargingDeadline()));
        Model.setReplyChargingId(ConvertUtils.getReplyChargingId(pdu.getReplyChargingId()));
        Model.setReplyChargingSize(ConvertUtils.getReplyChargingSize(pdu.getReplyChargingSize()));

        return Model;
    }

    public static SendReqModel GetModel(MySendReq pdu)
    {
        SendReqModel Model = new SendReqModel();

        Model.setBcc(ConvertUtils.getBcc(pdu.getBcc()));
        Model.setCc(ConvertUtils.getCc(pdu.getCc()));
        Model.setContentType(ConvertUtils.getContentType(pdu.getContentType()));
        Model.setMessageClass(ConvertUtils.getMessageClass(pdu.getMessageClass()));
        Model.setTransactionID(ConvertUtils.getTransactionId((pdu.getTransactionId())));
        Model.setDeliveryReport(ConvertUtils.getDeliveryReport(pdu.getDeliveryReport()));
        Model.setReadReport(ConvertUtils.getReadReport(pdu.getReadReport()));
        Model.setExpiry(ConvertUtils.getExpiry(pdu.getExpiry()));
        Model.setMessageSize(ConvertUtils.getMessageSize(pdu.getMessageSize()));
        Model.setTo(ConvertUtils.getTo(pdu.getTo()));
        Model.setDate(ConvertUtils.getDate(pdu.getDate()));
        Model.setSubject(ConvertUtils.getSubject(pdu.getSubject()));
        Model.setPriority(ConvertUtils.getPriority(pdu.getPriority()));
        Model.setMmsVersion(ConvertUtils.getMmsVersion(pdu.getMmsVersion()));
        Model.setFrom(ConvertUtils.getFrom(pdu.getFrom()));

        Model.setParts(GetPduBodyModel(pdu.getBody()));

        return Model;
    }


    public static SendConfModel GetModel(MySendConf pdu)
    {
        SendConfModel Model = new SendConfModel();


        Model.setMessageId(ConvertUtils.getMessageId(pdu.getMessageId()));
        Model.setTransactionId(ConvertUtils.getTransactionId(pdu.getTransactionId()));
        Model.setResponseStatus(ConvertUtils.getResponseStatus(pdu.getResponseStatus()));
        Model.setMmsVersion(ConvertUtils.getMmsVersion(pdu.getMmsVersion()));
        Model.setFrom(ConvertUtils.getFrom(pdu.getFrom()));


        Model.setContentLocation(ConvertUtils.getContentLocation(pdu.getContentLocation()));
        Model.setResponseText(ConvertUtils.getResponseText(pdu.getResponseText()));
        Model.setStoreStatus(ConvertUtils.getStoreStatus(pdu.getStoreStatus()));
        Model.setStoreStatusText(ConvertUtils.getStoreStatusText(pdu.getStoreStatusText()));


        return Model;
    }

    public static ArrayList<SmsReceiveModel> GetModel(ArrayList<SmsMessage> smsMessage, final ArrayList<String> bodys)
    {
        ArrayList<SmsReceiveModel> list = new ArrayList<>();

        for (SmsMessage msg : smsMessage)
        {
            list.add(GetModel(msg, bodys));
        }
        return list;
    }

    public static SmsReceiveModel GetModel(SmsMessage smsMessage, final ArrayList<String> bodys)
    {
        SmsReceiveModel smsReceive = new SmsReceiveModel();

        smsReceive.setServiceCenterAddress(smsMessage.getServiceCenterAddress());
        smsReceive.setOriginatingAddress(smsMessage.getOriginatingAddress());
        smsReceive.setDisplayOriginatingAddress(smsMessage.getDisplayOriginatingAddress());
        smsReceive.setMessageBody(smsMessage.getMessageBody());
        try
        {
            smsReceive.setMessageClass(ConvertUtils.getSmsClass(smsMessage.getMessageClass()));
        }
        catch (Exception e)
        {
        }
        smsReceive.setDisplayMessageBody(bodys);
        smsReceive.setPseudoSubject(smsMessage.getPseudoSubject());
        smsReceive.setTimestampMillis(smsMessage.getTimestampMillis());
        smsReceive.setEmail(smsMessage.isEmail());
        smsReceive.setEmailBody(smsMessage.getEmailBody());
        smsReceive.setEmailFrom(smsMessage.getEmailFrom());
        smsReceive.setProtocolIdentifier(smsMessage.getProtocolIdentifier());
        smsReceive.setReplace(smsMessage.isReplace());
        smsReceive.setCphsMwiMessage(smsMessage.isCphsMwiMessage());
        smsReceive.setMWIClearMessage(smsMessage.isMWIClearMessage());
        smsReceive.setMWISetMessage(smsMessage.isMWISetMessage());
        smsReceive.setMwiDontStore(smsMessage.isMwiDontStore());
        smsReceive.setUserData(smsMessage.getUserData());
        smsReceive.setStatusOnIcc(smsMessage.getStatusOnIcc());
        smsReceive.setIndexOnIcc(smsMessage.getIndexOnIcc());
        smsReceive.setStatus(smsMessage.getStatus());
        smsReceive.setStatusReportMessage(smsMessage.isStatusReportMessage());
        smsReceive.setReplyPathPresent(smsMessage.isReplyPathPresent());

        return smsReceive;
    }

    public static void fillSmsDeliveryReport(SmsDeliveryReport dr, SmsMessage smsMessage)
    {
        if (dr == null || smsMessage == null)
            return;


        try
        {
            dr.setServiceCenterAddress(smsMessage.getServiceCenterAddress());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setOriginatingAddress(smsMessage.getOriginatingAddress());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setDisplayOriginatingAddress(smsMessage.getDisplayOriginatingAddress());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setMessageBody(smsMessage.getMessageBody());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setMessageClass(ConvertUtils.getSmsClass(smsMessage.getMessageClass()));
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setPseudoSubject(smsMessage.getPseudoSubject());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setTimestampMillis(smsMessage.getTimestampMillis());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setEmail(smsMessage.isEmail());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setEmailBody(smsMessage.getEmailBody());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setEmailFrom(smsMessage.getEmailFrom());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setProtocolIdentifier(smsMessage.getProtocolIdentifier());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setReplace(smsMessage.isReplace());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setCphsMwiMessage(smsMessage.isCphsMwiMessage());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setMWIClearMessage(smsMessage.isMWIClearMessage());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setMWISetMessage(smsMessage.isMWISetMessage());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setMwiDontStore(smsMessage.isMwiDontStore());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setUserData(smsMessage.getUserData());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setStatusOnIcc(smsMessage.getStatusOnIcc());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setIndexOnIcc(smsMessage.getIndexOnIcc());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setStatus(smsMessage.getStatus());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setStatusReportMessage(smsMessage.isStatusReportMessage());
        }
        catch (Exception e)
        {
        }
        try
        {
            dr.setReplyPathPresent(smsMessage.isReplyPathPresent());
        }
        catch (Exception e)
        {
        }
    }
}

