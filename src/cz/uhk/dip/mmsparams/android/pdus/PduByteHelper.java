package cz.uhk.dip.mmsparams.android.pdus;

import android.content.Context;

import com.google.android.mms.ContentType;
import com.google.android.mms.pdu_alt.EncodedStringValue;
import com.google.android.mms.pdu_alt.GenericPdu;
import com.google.android.mms.pdu_alt.MyPduComposer;
import com.google.android.mms.pdu_alt.MyPduParser;
import com.google.android.mms.pdu_alt.MySendConf;
import com.google.android.mms.pdu_alt.MySendReq;
import com.google.android.mms.pdu_alt.PduBody;
import com.google.android.mms.pdu_alt.PduHeaders;
import com.google.android.mms.pdu_alt.PduPart;

import org.thoughtcrime.securesms.database.DatabaseFactory;
import org.thoughtcrime.securesms.logging.Log;
import org.thoughtcrime.securesms.mms.MmsSendResult;
import org.thoughtcrime.securesms.transport.UndeliverableMessageException;
import org.thoughtcrime.securesms.util.Hex;

import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import cz.uhk.dip.mmsparams.android.db.MessagePduDatabase;
import cz.uhk.dip.mmsparams.android.db.model.MessagePdu;
import cz.uhk.dip.mmsparams.android.utils.MmsParamsUtil;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;

public class PduByteHelper
{
    public static final String TAG = PduByteHelper.class.getSimpleName();

    public static final byte[] getPduBytes(@NonNull final Context context, @NonNull final GenericPdu pdu)
    {
        return new MyPduComposer(context, pdu).make();
    }

    @Nullable
    public static final <T extends GenericPdu> T getPdu(@NonNull final byte[] bytes)
    {
        if (bytes == null)
            return null;

        final GenericPdu pdu = new MyPduParser(bytes).parse();
        return (T) pdu;
    }

    public static long getPartSize(PduPart part)
    {
        return part.getName().length                //    123456789.jpg
                + part.getContentLocation().length   //   123456789.jpg
                + part.getContentType().length   //    image/jpeg
                + part.getData().length          //    pole bytů obsahu
                + part.getContentId().length;   //      <123456789.jpg>
    }


    public static void storeMessagePdu(@NonNull final Context context, @NonNull final long messageID, @NonNull final int type, @NonNull final byte[] nBytes)
    {
        MessagePduDatabase pduDatabase = DatabaseFactory.getmessagePduDatabase(context);
        pduDatabase.insert(new MessagePdu(1, messageID, type, nBytes));
    }

    public static MmsSendResult getSendResult(MySendConf conf, MySendReq message)
            throws UndeliverableMessageException
    {
        if (conf == null)
        {
            throw new UndeliverableMessageException("No M-Send.conf received in response to send.");
        }
        else if (conf.getResponseStatus() != PduHeaders.RESPONSE_STATUS_OK)
        {
            throw new UndeliverableMessageException("Got bad response: " + conf.getResponseStatus());
        }
        else if (isInconsistentResponse(message, conf))
        {
            throw new UndeliverableMessageException("Mismatched response!");
        }
        else
        {
            return new MmsSendResult(conf.getMessageId(), conf.getResponseStatus());
        }
    }

    private static boolean isInconsistentResponse(MySendReq message, MySendConf response)
    {
        Log.w(TAG, "Comparing: " + Hex.toString(message.getTransactionId()));
        Log.w(TAG, "With:      " + Hex.toString(response.getTransactionId()));
        return !Arrays.equals(message.getTransactionId(), response.getTransactionId());
    }


    public static String getSizeString(long size)
    {
        if (size <= 0)
            return String.valueOf(size);


        return MmsParamsUtil.getPrettyFileSize(size) + " (" + String.valueOf(size) + ")";
    }

    public static long getPduBodySize(final PduBody body, final EncodedStringValue subject)
    {
        if (body == null)
        {
            return -1;
        }

        int size = body.getPartsNum();

        long result = 0;
        for (int i = 0; i < size; i++)
        {
            final PduPart part = body.getPart(i);


            byte[] ct = part.getContentType();
            if (ct == null)
                continue;

            if (ContentType.APP_SMIL.equals(StringUtil.getString(ct)))
            {
                continue;
            }

            result += getPartSize(part);
        }

        if (subject != null && !StringUtil.isEmptyOrNull(subject.getString()))
        {
            result += subject.getString().length();
        }

        return result;
    }
}
