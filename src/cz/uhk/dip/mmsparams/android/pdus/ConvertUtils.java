package cz.uhk.dip.mmsparams.android.pdus;

import android.net.Uri;
import android.telephony.SmsMessage;

import com.google.android.mms.pdu_alt.EncodedStringValue;

import cz.uhk.dip.mmsparams.api.enums.SmsClass;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;

public class ConvertUtils
{
    public static String getBytesToString(byte[] bytes)
    {
        return StringUtil.getString(bytes);
    }

    private static String getESToString(EncodedStringValue es)
    {
        if (es == null)
            return null;

        return es.getString();
    }

    private static String[] getNumberToString(EncodedStringValue[] es)
    {
        if (es == null || es.length == 0)
            return null;

        String[] result = new String[es.length];

        for (int i = 0; i < es.length; i++)
        {
            result[i] = getESToString(es[i]);
        }
        return result;
    }

    public static String getTransactionId(byte[] transactionId)
    {
        return getBytesToString(transactionId);
    }

    public static String getMessageId(byte[] messageId)
    {
        return getBytesToString(messageId);
    }

    public static String getFrom(EncodedStringValue from)
    {
        return getESToString(from);
    }

    public static int getMmsVersion(int mmsVersion)
    {
        return mmsVersion;
    }

    public static int getReportAllowed(int reportAllowed)
    {
        return reportAllowed;
    }

    public static Long getDate(long date)
    {
        return date;
    }

    public static int getStatus(int status)
    {
        return status;
    }

    public static String[] getTo(EncodedStringValue[] to)
    {
        return getNumberToString(to);
    }

    public static int getContentClass(int contentClass)
    {
        return contentClass;
    }

    public static int getReadReport(int readReport)
    {
        return readReport;
    }

    public static String getRetrieveText(EncodedStringValue retrieveText)
    {
        return getESToString(retrieveText);
    }

    public static int getRetrieveStatus(int retrieveStatus)
    {
        return retrieveStatus;
    }

    public static int getPriority(int priority)
    {
        return priority;
    }

    public static int getDeliveryReport(int deliveryReport)
    {
        return deliveryReport;
    }

    public static String getMessageClass(byte[] messageClass)
    {
        return getBytesToString(messageClass);
    }

    public static String getContentType(byte[] contentType)
    {
        return getBytesToString(contentType);
    }

    public static String[] getBcc(EncodedStringValue[] bcc)
    {
        return getNumberToString(bcc);
    }

    public static String[] getCc(EncodedStringValue[] cc)
    {
        return getNumberToString(cc);
    }

    public static Long getExpiry(long expiry)
    {
        return expiry;
    }

    public static Long getMessageSize(long messageSize)
    {
        return messageSize;
    }

    public static int getResponseStatus(int responseStatus)
    {
        return responseStatus;
    }

    public static String getSubject(EncodedStringValue subject)
    {
        return getESToString(subject);
    }

    public static int getReadStatus(int readStatus)
    {
        return readStatus;
    }

    public static String getContentLocation(byte[] contentLocation)
    {
        return getBytesToString(contentLocation);
    }

    public static int getCharset(int charset)
    {
        return charset;
    }

    public static String getContentId(byte[] contentId)
    {
        return getBytesToString(contentId);
    }

    public static String getName(byte[] name)
    {
        return getBytesToString(name);
    }

    public static String getContentDisposition(byte[] contentDisposition)
    {
        return getBytesToString(contentDisposition);
    }

    public static String getContentTransferEncoding(byte[] contentTransferEncoding)
    {
        return getBytesToString(contentTransferEncoding);
    }

    public static String getFilename(byte[] filename)
    {
        return getBytesToString(filename);
    }

    public static int getDataLength(int dataLength)
    {
        return dataLength;
    }

    public static String generateLocation(String s)
    {
        return s;
    }

    public static String getDataUri(Uri dataUri)
    {
        if (dataUri == null)
            return null;
        // TODO check - debug return type of toString
        return dataUri.toString();
    }

    public static byte[] getData(byte[] data)
    {
        return data;
    }

    public static byte[] getApplicId(byte[] applicId)
    {
        return applicId;
    }

    public static byte[] getAuxApplicId(byte[] auxApplicId)
    {
        return auxApplicId;
    }

    public static byte[] getReplyApplicId(byte[] replyApplicId)
    {
        return replyApplicId;
    }

    public static String getStatusText(EncodedStringValue statusText)
    {
        return getESToString(statusText);
    }

    public static int getDistributionIndicator(int distributionIndicator)
    {
        return distributionIndicator;
    }

    public static int getRecommendedRetrievalMode(int recommendedRetrievalMode)
    {
        return recommendedRetrievalMode;
    }

    public static int getRecommendedRetrievalModeText(int recommendedRetrievalModeText)
    {
        return recommendedRetrievalModeText;
    }

    public static byte[] getReplaceId(byte[] replaceId)
    {
        return replaceId;
    }

    public static int getReplyCharging(int replyCharging)
    {
        return replyCharging;
    }

    public static int getReplyChargingDeadline(int replyChargingDeadline)
    {
        return replyChargingDeadline;
    }

    public static byte[] getReplyChargingId(byte[] replyChargingId)
    {
        return replyChargingId;
    }

    public static long getReplyChargingSize(long replyChargingSize)
    {
        return replyChargingSize;
    }

    public static int getStored(int stored)
    {
        return stored;
    }

    public static int getDrmContent(int drmContent)
    {
        return drmContent;
    }

    public static String getResponseText(EncodedStringValue responseText)
    {
        return getESToString(responseText);
    }

    public static int getStoreStatus(int storeStatus)
    {
        return storeStatus;
    }

    public static byte[] getStoreStatusText(byte[] storeStatusText)
    {
        return storeStatusText;
    }

    public static SmsClass getSmsClass(SmsMessage.MessageClass messageClass)
    {
        if (messageClass == null)
            return null;

        switch (messageClass)
        {
            case CLASS_0:
                return SmsClass.CLASS_0;
            case CLASS_1:
                return SmsClass.CLASS_1;
            case CLASS_2:
                return SmsClass.CLASS_2;
            case CLASS_3:
                return SmsClass.CLASS_3;
            case UNKNOWN:
                return SmsClass.UNKNOWN;
            default:
                return null;
        }
    }
}

