package cz.uhk.dip.mmsparams.android.exception;

public class EmptyException extends Throwable
{
    public EmptyException()
    {
    }

    public EmptyException(String message)
    {
        super(message);
    }
}
