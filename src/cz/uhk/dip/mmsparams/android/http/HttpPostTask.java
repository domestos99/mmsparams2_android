package cz.uhk.dip.mmsparams.android.http;


import android.os.AsyncTask;

import cz.uhk.dip.mmsparams.api.http.HttpPostUtil;
import cz.uhk.dip.mmsparams.api.http.TaskResult;

public class HttpPostTask
{
    public static AsyncTask<String, Void, TaskResult<String>> post()
    {
        return new PostDataTask();
    }

    public static class PostDataTask extends AsyncTask<String, Void, TaskResult<String>>
    {
        private static final String TAG = PostDataTask.class.getName();

        @Override
        protected TaskResult<String> doInBackground(String... strings)
        {
            return getJSON(strings[0], strings[1]);
        }

        private TaskResult<String> getJSON(String urlAddress, String data)
        {
            return HttpPostUtil.postData(urlAddress, data);
        }
    }
}
