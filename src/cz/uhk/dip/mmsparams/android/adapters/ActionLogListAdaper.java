package cz.uhk.dip.mmsparams.android.adapters;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import cz.uhk.dip.mmsparams.android.R;
import org.thoughtcrime.securesms.util.ViewUtil;

import java.util.List;

import cz.uhk.dip.mmsparams.android.R;
import cz.uhk.dip.mmsparams.android.adapters.base.MyAdapterBase;
import cz.uhk.dip.mmsparams.android.db.model.ActionLogModel;

public class ActionLogListAdaper extends MyAdapterBase<ActionLogModel>
{

    private static final int row_layout_ID = R.layout.action_log_browse_item;

    public ActionLogListAdaper(Context context, List<ActionLogModel> objects)
    {
        super(context, row_layout_ID, objects);

    }

    @Override
    protected int getResource()
    {
        return row_layout_ID;
    }

    @Override
    protected void setupView(View view, ActionLogModel obj, int position)
    {

        TextView tv = ViewUtil.findById(view, R.id.name);
        tv.setText(obj.getTag());

        TextView info = ViewUtil.findById(view, R.id.info);
        info.setText(obj.getMessage());
    }
}
