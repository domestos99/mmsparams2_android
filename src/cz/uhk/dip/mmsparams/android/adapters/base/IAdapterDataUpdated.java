package cz.uhk.dip.mmsparams.android.adapters.base;

import java.util.List;

public interface IAdapterDataUpdated<T>
{
    void Update(List<T> list);
}

