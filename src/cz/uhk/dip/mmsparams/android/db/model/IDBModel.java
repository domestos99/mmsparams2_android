package cz.uhk.dip.mmsparams.android.db.model;

import android.content.ContentValues;

public interface IDBModel<T>
{
    int getID();

    ContentValues getContentValues(T obj);


    void setIDNegative();

    void setCreatedDT(long dt);

    void setUpdatedDT(long dt);
}
