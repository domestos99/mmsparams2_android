package cz.uhk.dip.mmsparams.android.db.model;

import android.content.ContentValues;
import android.database.Cursor;

public class ActionLogModel implements IDBModel<ActionLogModel>
{

    private static final String TAG = ActionLogModel.class.getSimpleName();

    private int id;

    private String tag;
    private String message;
    private String body;

    private long dateCreated;

    public static ActionLogModel getFromCursor(Cursor cursor)
    {
        ActionLogModel model = new ActionLogModel();

        model.id = cursor.getInt(cursor.getColumnIndexOrThrow(ActionLogModel.MetaData.ID));

        model.tag = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.TAG));
        model.message = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.MESSAGE));
        model.body = cursor.getString(cursor.getColumnIndexOrThrow(MetaData.BODY));

        model.dateCreated = cursor.getLong(cursor.getColumnIndexOrThrow(MetaData.DATE_CREATED));


        return model;
    }


    public static class MetaData
    {
        public static final String ID = "_id";
        public static final String TAG = "tag";
        public static final String MESSAGE = "message";
        public static final String BODY = "body";


        public static final String DATE_CREATED = "date_created";

    }


    @Override
    public int getID()
    {
        return id;
    }

    @Override
    public ContentValues getContentValues(ActionLogModel instance)
    {
        ContentValues cv = new ContentValues();

        // add id ???

        cv.put(MetaData.TAG, instance.getTag());
        cv.put(MetaData.MESSAGE, instance.getMessage());
        cv.put(MetaData.BODY, instance.getBody());
        cv.put(ActionLogModel.MetaData.DATE_CREATED, instance.getDateCreated());


        return cv;
    }

    @Override
    public void setIDNegative()
    {
        this.id = -1;
    }

    @Override
    public void setCreatedDT(long dt)
    {
        this.dateCreated = dt;
    }

    @Override
    public void setUpdatedDT(long dt)
    {

    }


    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getTag()
    {
        return tag;
    }

    public void setTag(String tag)
    {
        this.tag = tag;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public long getDateCreated()
    {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated)
    {
        this.dateCreated = dateCreated;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }
}

