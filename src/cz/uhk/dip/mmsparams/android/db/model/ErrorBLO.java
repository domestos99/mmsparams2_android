package cz.uhk.dip.mmsparams.android.db.model;

import java.io.Serializable;

public class ErrorBLO implements Serializable
{
    public String tag;
    public String info;
    public String message;
    public String stackTrace;
}
