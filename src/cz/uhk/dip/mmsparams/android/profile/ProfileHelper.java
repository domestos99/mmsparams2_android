package cz.uhk.dip.mmsparams.android.profile;

import android.content.Context;

import cz.uhk.dip.mmsparams.android.preferences.MmsParamsPreferences;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsRecipientPhoneProfile;

public class ProfileHelper
{
    public static void setRecipientProfile(final Context context, final MmsRecipientPhoneProfile mmsRecipientPhoneProfile)
    {
        MmsParamsPreferences.setRecipientProfile(context, mmsRecipientPhoneProfile);
    }
}
