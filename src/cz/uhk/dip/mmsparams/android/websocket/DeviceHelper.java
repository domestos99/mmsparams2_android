package cz.uhk.dip.mmsparams.android.websocket;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import java.util.ArrayList;
import java.util.List;

import androidx.core.app.ActivityCompat;
import cz.uhk.dip.mmsparams.android.preferences.MmsParamsPreferences;
import cz.uhk.dip.mmsparams.api.interfaces.IPhoneInfoProvider;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.PhoneInfoModel;
import cz.uhk.dip.mmsparams.api.websocket.model.phone.SubscriptionInfoModel;

public class DeviceHelper implements IPhoneInfoProvider
{
    private final Context context;

    public DeviceHelper(Context context)
    {
        this.context = context;
    }

    @Override
    public PhoneInfoModel getPhoneInfo()
    {
        return generateDeviceInfo(this.context);
    }

    public static PhoneInfoModel generateDeviceInfo(final Context context)
    {
        List<SubscriptionInfoModel> subscriptionInfoModel = new ArrayList<>();

        try
        {
            SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
            List<SubscriptionInfo> subscriptionInfos = subscriptionManager.getActiveSubscriptionInfoList();

            for (SubscriptionInfo si : subscriptionInfos)
            {
                subscriptionInfoModel.add(generateSIModel(si));
            }
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }


        PhoneInfoModel di = new PhoneInfoModel();
        setDeviceInfo(di);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)
        {
            TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String imsi = mTelephonyMgr.getSubscriberId();
            di.setImsi(imsi);
        }

        di.setPhoneCustomName(MmsParamsPreferences.getPhoneCustomName(context));
        di.setPhoneNumber(MmsParamsPreferences.getPhoneNumber(context));

        SubscriptionInfoModel[] arr = new SubscriptionInfoModel[subscriptionInfoModel.size()];
        for (int i = 0; i < subscriptionInfoModel.size(); i++)
            arr[i] = subscriptionInfoModel.get(i);


        di.setSubscriptionInfoModel(arr);
        return di;
    }

    private static SubscriptionInfoModel generateSIModel(SubscriptionInfo si)
    {
        if (si == null)
            return null;
        SubscriptionInfoModel subscriptionInfoModel = new SubscriptionInfoModel();

        subscriptionInfoModel.setSubscriptionId(si.getSubscriptionId());
        subscriptionInfoModel.setIccId(si.getIccId());
        subscriptionInfoModel.setSimSlotIndex(si.getSimSlotIndex());
        subscriptionInfoModel.setDisplayName(si.getDisplayName() == null ? null : si.getDisplayName().toString());
        subscriptionInfoModel.setCarrierName(si.getCarrierName() == null ? null : si.getCarrierName().toString());
        // subscriptionInfoModel.setateIconBitmap(si.createIconBitmap(Context context));
        subscriptionInfoModel.setIconTint(si.getIconTint());
        subscriptionInfoModel.setNumber(si.getNumber());
        subscriptionInfoModel.setDataRoaming(si.getDataRoaming());
        subscriptionInfoModel.setMcc(si.getMcc());
        subscriptionInfoModel.setMnc(si.getMnc());
        subscriptionInfoModel.setCountryIso(si.getCountryIso());

        return subscriptionInfoModel;
    }


    private static void setDeviceInfo(PhoneInfoModel di)
    {
        di.setSerial(Build.SERIAL);
        di.setModel(Build.MODEL);
        di.setId(Build.ID);
        di.setManufacturer(Build.MANUFACTURER);
        di.setBrand(Build.BRAND);
        di.setType(Build.TYPE);
        di.setUser(Build.USER);
        di.setCodesBase(Build.VERSION_CODES.BASE);
        di.setVersionIncremental(Build.VERSION.INCREMENTAL);
        di.setVersionSdk(Build.VERSION.SDK);
        di.setVersionSdkInt(Build.VERSION.SDK_INT);
        di.setBoard(Build.BOARD);
        di.setBrand(Build.BRAND);
        di.setHost(Build.HOST);
        di.setFingerprint(Build.FINGERPRINT);
        di.setRelease(Build.VERSION.RELEASE);
        di.setPhoneKey(WebSocketSingleton.getSenderKey());
    }



}
