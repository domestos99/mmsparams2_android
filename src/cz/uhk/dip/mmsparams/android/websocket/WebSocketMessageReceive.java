package cz.uhk.dip.mmsparams.android.websocket;

import android.content.Context;

import org.thoughtcrime.securesms.logging.Log;

import cz.uhk.dip.mmsparams.android.facades.WebSocketLoggerFacade;
import cz.uhk.dip.mmsparams.api.websocket.router.MessageRouter;
import okhttp3.WebSocket;
import okio.ByteString;

public class WebSocketMessageReceive implements IWebSocketMessageReceive
{
    private static final String TAG = WebSocketMessageReceive.class.getSimpleName();

    private final Context context;
    private final MessageReceiveSub messageReceiveSub;

    public WebSocketMessageReceive(final Context context)
    {
        this.context = context;
        this.messageReceiveSub = new MessageReceiveSub(context);
    }

    @Override
    public void onMessage(WebSocket webSocket, String message)
    {
        WebSocketLoggerFacade.logIncomingMessage(context, message);

        try
        {
            MessageRouter.process(messageReceiveSub, message);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Log.e(TAG, "Error in onMessage receive", ex);
        }
    }

    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes)
    {
        // throw new SystemException("WebSocketMessageReceive - byteString");
    }
}
