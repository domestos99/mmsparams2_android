package cz.uhk.dip.mmsparams.android.websocket;

import android.content.Context;

import cz.uhk.dip.mmsparams.android.facades.LoggerFacade;
import okhttp3.Response;
import okhttp3.WebSocket;
import okio.ByteString;

public class WebSocketActionNotifier implements IWebSocketActionNotifier
{
    private static final String TAG = WebSocketActionNotifier.class.getSimpleName();
    private final Context context;

    public WebSocketActionNotifier(Context context)
    {
        this.context = context;
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response)
    {
        output("Open");
    }

    @Override
    public void onMessage(WebSocket webSocket, String text)
    {
        output("Receiving : " + text);
    }

    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes)
    {
        output("Receiving : " + bytes.toString());
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason)
    {
        output("Closing : " + code + " / " + reason);
    }

    @Override
    public void onClosed(WebSocket webSocket, int code, String reason)
    {
        output("onClosed : " + code + " / " + reason);
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response)
    {
        output("Error : " + t.getMessage());
    }


    private void output(String msg)
    {
        LoggerFacade.log(TAG, msg);
    }



}
