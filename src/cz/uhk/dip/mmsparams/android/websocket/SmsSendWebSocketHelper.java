package cz.uhk.dip.mmsparams.android.websocket;

import android.content.Context;

import org.thoughtcrime.securesms.ApplicationContext;
import org.thoughtcrime.securesms.database.Address;
import org.thoughtcrime.securesms.database.DatabaseFactory;
import org.thoughtcrime.securesms.database.SmsDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.sms.OutgoingTextMessage;

import cz.uhk.dip.mmsparams.android.jobs.SmsSendWSJob;
import cz.uhk.dip.mmsparams.api.websocket.messages.sms.SmsSendPhoneRequestMessage;

public class SmsSendWebSocketHelper
{
    public static void process(Context context, SmsSendPhoneRequestMessage req)
    {
        SmsDatabase db = DatabaseFactory.getSmsDatabase(context);

        Recipient recipient = Recipient.from(context, Address.fromExternal(context, req.getSmsSendModel().getTo()), false);
        int subscriptionId = recipient.getDefaultSubscriptionId().or(-1);

        long threadId = DatabaseFactory.getThreadDatabase(context).getThreadIdFor(recipient);

        OutgoingTextMessage msg = new OutgoingTextMessage(recipient, req.getSmsSendModel().getText(), subscriptionId);


        long messageId = db.insertMessageOutbox(threadId, msg, false, System.currentTimeMillis(), null);


        // Request to send SMS
        ApplicationContext.getInstance(context)
                .getJobManager()
                .add(new SmsSendWSJob(context, messageId, recipient.getAddress(), req));
    }
}
