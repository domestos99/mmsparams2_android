package cz.uhk.dip.mmsparams.android.websocket;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketCloseStatus;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public final class WebSocketHandler extends WebSocketListener
{
    private static final String TAG = WebSocketHandler.class.getSimpleName();

    private final IWebSocketMessageReceive iWebSocketMessageReceive;
    private final IWebSocketActionNotifier iWebSocketActionNotifier;


    public WebSocketHandler(final IWebSocketMessageReceive iWebSocketMessageReceive, final IWebSocketActionNotifier iWebSocketActionNotifier)
    {
        this.iWebSocketMessageReceive = iWebSocketMessageReceive;
        this.iWebSocketActionNotifier = iWebSocketActionNotifier;
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response)
    {
        WebSocketSingleton.getInstance().onOpen(webSocket, response);
        if (iWebSocketActionNotifier != null)
            iWebSocketActionNotifier.onOpen(webSocket,  response);
    }

    @Override
    public void onMessage(WebSocket webSocket, String text)
    {
        if (iWebSocketActionNotifier != null)
            iWebSocketActionNotifier.onMessage(webSocket,  text);
        if (iWebSocketMessageReceive != null)
            iWebSocketMessageReceive.onMessage(webSocket, text);
    }

    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes)
    {
        if (iWebSocketActionNotifier != null)
            iWebSocketActionNotifier.onMessage(webSocket,  bytes);
        this.onMessage(webSocket, bytes.hex());
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason)
    {
        WebSocketSingleton.getInstance().onClosing(webSocket, code, reason);
        if (iWebSocketActionNotifier != null)
            iWebSocketActionNotifier.onClosing(webSocket, code, reason);
        webSocket.close(WebSocketCloseStatus.NORMAL, null);
    }

    @Override
    public void onClosed(WebSocket webSocket, int code, String reason)
    {
        WebSocketSingleton.getInstance().onClose(webSocket, code, reason);
        if (iWebSocketActionNotifier != null)
            iWebSocketActionNotifier.onClosed(webSocket, code, reason);
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response)
    {
        WebSocketSingleton.getInstance().onFailure(webSocket, t, response);
        if (iWebSocketActionNotifier != null)
            iWebSocketActionNotifier.onFailure(webSocket,  t, response);
    }
}

