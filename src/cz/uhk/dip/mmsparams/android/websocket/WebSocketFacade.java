package cz.uhk.dip.mmsparams.android.websocket;

import cz.uhk.dip.mmsparams.api.websocket.WebSocketConstants;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;

public class WebSocketFacade
{
    private static final String TAG = WebSocketFacade.class.getSimpleName();

    @Deprecated
    public static void send(final WebSocketMessageBase message, final boolean isResponse)
    {
        WebSocketSingleton.getInstance().sendMessage(message, isResponse);
    }

    public static void sendClientLibBroadcast(final WebSocketMessageBase message, final boolean isResponse)
    {
        message.setRecipientKey(WebSocketConstants.Server_To_Client_Broadcast);
        WebSocketSingleton.getInstance().sendMessage(message, isResponse);
    }

//    public static void sendResposne(WebSocketMessageBase message, WebSocketMessageBase request)
//    {
//        message = MessageUtils.prepareResponse(message, request);
//
//        WebSocketSingleton.getInstance().sendMessage(message, true);
//    }

    public static void sendToServer(final WebSocketMessageBase message, final boolean isResponse)
    {
        message.setRecipientKey(WebSocketConstants.Server_Recipient_Key);
        WebSocketSingleton.getInstance().sendMessage(message, isResponse);
    }
}
