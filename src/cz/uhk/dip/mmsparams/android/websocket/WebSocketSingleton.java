package cz.uhk.dip.mmsparams.android.websocket;

import android.content.Context;

import org.thoughtcrime.securesms.logging.Log;

import java.util.Timer;

import cz.uhk.dip.mmsparams.android.apiinterface.AuthClient;
import cz.uhk.dip.mmsparams.android.devicekey.DeviceIdSingleton;
import cz.uhk.dip.mmsparams.android.facades.LoggerFacade;
import cz.uhk.dip.mmsparams.android.facades.WebSocketLoggerFacade;
import cz.uhk.dip.mmsparams.android.preferences.MmsParamsPreferences;
import cz.uhk.dip.mmsparams.api.constants.HttpConstants;
import cz.uhk.dip.mmsparams.api.http.auth.JwtResponse;
import cz.uhk.dip.mmsparams.api.utils.StringUtil;
import cz.uhk.dip.mmsparams.api.websocket.MessageIdGenerator;
import cz.uhk.dip.mmsparams.api.websocket.MessageUtils;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketCloseStatus;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketConstants;
import cz.uhk.dip.mmsparams.api.websocket.WebSocketMessageBase;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;

public class WebSocketSingleton
{
    private static WebSocketSingleton instance = null;
    private static final String TAG = WebSocketSingleton.class.getSimpleName();

    private OkHttpClient client;
    private WebSocket ws;
    private IWebSocketMessageReceive iWebSocketMessageReceive;
    private IWebSocketActionNotifier iWebSocketActionNotifier;
    private Context context;

    private boolean isInitialized = false;
    private boolean isSocketOpen = false;
    private Timer timerAlive;

    protected WebSocketSingleton()
    {
        // Exists only to defeat instantiation.
    }

    public static WebSocketSingleton getInstance()
    {
        if (instance == null)
        {
            instance = new WebSocketSingleton();
        }
        return instance;
    }

    public synchronized void init(Context context, String serverAddress) throws Exception
    {
        if (isInitialized)
            return;

        this.context = context;
        this.timerAlive = new Timer();
        this.lastError = "";
        client = new OkHttpClient();
        iWebSocketMessageReceive = new WebSocketMessageReceive(context);
        iWebSocketActionNotifier = new WebSocketActionNotifier(context);
        connectToServer(serverAddress);
        isInitialized = true;
    }

    private void connectToServer(String serverAddress) throws Exception
    {
        JwtResponse token = MmsParamsPreferences.getJwtResponse(this.context);
        if (token == null)
        {
            try
            {
                token = new AuthClient(context).auth(MmsParamsPreferences.getWSConnectSettings(context));
                MmsParamsPreferences.setJwtResponse(context, token);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                throw e;
            }
        }

        Request request = new Request.Builder()
                .url(serverAddress)
                .addHeader(HttpConstants.AUTHORIZATION, HttpConstants.getAuthHeaderValue(token.getToken()))
                .build();
        WebSocketHandler listener = new WebSocketHandler(iWebSocketMessageReceive, iWebSocketActionNotifier);
        ws = client.newWebSocket(request, listener);
        client.dispatcher().executorService().shutdown();
    }

    public void sendMessage(final WebSocketMessageBase message, final boolean isResponse)
    {
        if (ws == null)
        {
            Log.e(TAG, "WebSocket cannot be empty");
            return;
        }
        if (message == null)
        {
            Log.e(TAG, "Message cannot be empty");
            return;
        }

        message.setSenderKey(getSenderKey());

        if (!isResponse)
            message.setMessageID(generateMessageKey());

        String json = MessageUtils.getSendableMessage(message);

        if (StringUtil.isEmptyOrNull(json))
        {
            Log.e(TAG, "Message cannot be empty");
            return;
        }

        LoggerFacade.log(TAG, "Sending: " + message);
        WebSocketLoggerFacade.logOutgoingMessage(context, TAG, message, isResponse);
        // TODO
        // MessageListFacade.insertSendingMsg(message);
        ws.send(json);
    }

    public static String getSenderKey()
    {
        return WebSocketConstants.Android_Key_Prefix + DeviceIdSingleton.getInstance().getDeviKey();
    }

    private static String generateMessageKey()
    {
        return WebSocketConstants.Android_Key_Prefix + MessageIdGenerator.getNext();
    }


    public synchronized void disconnect()
    {
        if (ws != null)
        {
            ws.close(WebSocketCloseStatus.NORMAL, "Android is disconnecting");
            this.isInitialized = false;
        }
    }

    public void onOpen(WebSocket webSocket, Response response)
    {
        this.isSocketOpen = true;
        this.lastError = "";
        setupTimer();
    }

    private void setupTimer()
    {
        if (MmsParamsPreferences.getSendKeepAliveMessage(context))
        {
            Log.d(TAG, "Setting scheduleAtFixedRate for KeepAliveMessage");
            timerAlive.scheduleAtFixedRate(new SendAliveMessageTask(), 0, 60 * 1000);
        }
    }

    public synchronized void onClosing(WebSocket webSocket, int code, String reason)
    {
        this.isSocketOpen = false;
        this.isInitialized = false;
        Log.e(TAG, reason);
        this.timerAlive.cancel();
        this.lastError = code + " - " + reason;
    }

    public synchronized void onClose(WebSocket webSocket, int code, String reason)
    {
        this.isSocketOpen = false;
        this.isInitialized = false;
        Log.e(TAG, reason);
        this.timerAlive.cancel();
        this.lastError = code + " - " + reason;
    }

    public synchronized void onFailure(WebSocket webSocket, Throwable t, Response response)
    {
        this.isSocketOpen = false;
        this.isInitialized = false;
        Log.e(TAG, response == null ? "" : response.toString(), t);
        this.timerAlive.cancel();
        if (t != null)
            this.lastError = t.getMessage();
    }

    private String lastError;

    public String getLastError()
    {
        return lastError;
    }

    public synchronized boolean isOpenx()
    {
        return isInitialized;
    }

    public boolean isSocketOpen()
    {
        return isSocketOpen;
    }
}
