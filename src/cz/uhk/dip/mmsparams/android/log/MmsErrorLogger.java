package cz.uhk.dip.mmsparams.android.log;

import android.content.Context;

import org.thoughtcrime.securesms.database.DatabaseFactory;

import cz.uhk.dip.mmsparams.android.db.MmsErrorDatabase;

public class MmsErrorLogger
{
    private final Context context;
    private final MmsErrorDatabase mmsErrorDatabase;
    private long messageID;

    public MmsErrorLogger(Context context)
    {
        this.context = context;
        this.mmsErrorDatabase = DatabaseFactory.getMmsErrorDatabase(context);
    }

    public void setMessageID(long messageID)
    {
        this.messageID = messageID;
    }

    public void log(String tag, Throwable e)
    {
        // TODO
        if (this.messageID < 0)
            return;

        mmsErrorDatabase.insertError(this.messageID, tag, e);
    }

    public void log(long messageId, String tag, Throwable e)
    {
        mmsErrorDatabase.insertError(messageId, tag, e);
    }

    public void log(long messageId, String tag, String extraInfo, Throwable e)
    {
        mmsErrorDatabase.insertError(messageId, tag, extraInfo, e);
    }

}
