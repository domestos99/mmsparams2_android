package cz.uhk.dip.mmsparams.android.log;

import android.content.Context;

import org.thoughtcrime.securesms.database.DatabaseFactory;
import org.thoughtcrime.securesms.logging.Log;

import cz.uhk.dip.mmsparams.android.db.ErrorLogDatabase;
import cz.uhk.dip.mmsparams.android.db.model.ErrorLog;

public class DBLog
{
    public static void logToDB(Context context, Throwable e)
    {
        logToDB(context, "DBLog", e);
    }

    public static void logToDB(Context context, String tag, Throwable e)
    {
        logToDB(context, "", tag, e);
    }


    public static void logToDB(Context context, String massage, String tag, Throwable e)
    {
        try
        {
            Log.e(tag, "logToDB", e);

            ErrorLogDatabase db = DatabaseFactory.getErrorLogDatabase(context);
            db.insert(ErrorLog.convert(e, massage));

        }
        catch (Exception ex)
        {
            Log.e(tag, "logToDB", e);
            Log.e(tag, "logToDB", ex);
        }
    }

}
