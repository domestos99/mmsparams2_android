package cz.uhk.dip.mmsparams.android.jobs;

import org.thoughtcrime.securesms.ApplicationContext;
import org.thoughtcrime.securesms.database.DatabaseFactory;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobs.BaseJob;
import org.thoughtcrime.securesms.jobs.MmsDownloadJob;
import org.thoughtcrime.securesms.notifications.MessageNotifier;
import org.thoughtcrime.securesms.service.KeyCachingService;

import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;

public class MmsDownloadWSManualJob extends BaseJob
{

    public static final String KEY = MmsDownloadWSManualJob.class.getSimpleName();

    private static final String TAG = MmsDownloadWSManualJob.class.getSimpleName();

    private static final String KEY_MESSAGE_ID = "message_id";
    private static final String KEY_THREAD_ID = "thread_id";
    private static final String KEY_AUTOMATIC = "automatic";
    private static final String KEY_downloadAfterSec = "downloadAfterSec";

    private final long messageId;
    private final long threadId;
    private final boolean automatic;
    private final int downloadAfterSec;

    public MmsDownloadWSManualJob(long messageId, long threadId, boolean automatic, int downloadAfterSec)
    {
        this(new Job.Parameters.Builder()
                        .setQueue("mms-operation")
                        .setMaxAttempts(25)
                        .build(),
                messageId,
                threadId,
                automatic, downloadAfterSec);

    }

    private MmsDownloadWSManualJob(@NonNull Job.Parameters parameters, long messageId, long threadId, boolean automatic, int downloadAfterSec)
    {
        super(parameters);

        this.messageId = messageId;
        this.threadId = threadId;
        this.automatic = automatic;
        this.downloadAfterSec = downloadAfterSec;
    }

    @Override
    public @NonNull
    Data serialize()
    {
        return new Data.Builder()
                .putLong(KEY_MESSAGE_ID, messageId)
                .putLong(KEY_THREAD_ID, threadId)
                .putBoolean(KEY_AUTOMATIC, automatic)
                .putInt(KEY_downloadAfterSec, downloadAfterSec)
                .build();
    }

    @Override
    public @NonNull
    String getFactoryKey()
    {
        return KEY;
    }

    @Override
    public void onAdded()
    {
        if (automatic && KeyCachingService.isLocked(context))
        {
            DatabaseFactory.getMmsDatabase(context).markIncomingNotificationReceived(threadId);
            MessageNotifier.updateNotification(context);
        }
    }

    @Override
    public void onRun()
    {
        new Timer().schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                ApplicationContext.getInstance(context)
                        .getJobManager()
                        .add(new MmsDownloadJob(
                                messageId,
                                threadId,
                                automatic, true, true));
            }
        }, downloadAfterSec * 1000);
    }

    @Override
    public void onCanceled()
    {

    }

    @Override
    public boolean onShouldRetry(@NonNull Exception exception)
    {
        return false;
    }

    public static final class Factory implements Job.Factory<MmsDownloadWSManualJob>
    {
        @Override
        public @NonNull
        MmsDownloadWSManualJob create(@NonNull Job.Parameters parameters, @NonNull Data data)
        {
            return new MmsDownloadWSManualJob(parameters,
                    data.getLong(KEY_MESSAGE_ID),
                    data.getLong(KEY_THREAD_ID),
                    data.getBoolean(KEY_AUTOMATIC),
                    data.getInt(KEY_downloadAfterSec));
        }
    }
}
