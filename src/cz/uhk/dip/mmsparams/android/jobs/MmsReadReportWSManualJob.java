package cz.uhk.dip.mmsparams.android.jobs;

import android.content.Context;

import com.google.android.mms.pdu_alt.MyRetrieveConf;
import com.google.android.mms.pdu_alt.PduHeaders;

import org.thoughtcrime.securesms.database.DatabaseFactory;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.jobs.BaseJob;
import org.thoughtcrime.securesms.logging.Log;
import org.thoughtcrime.securesms.notifications.MessageNotifier;
import org.thoughtcrime.securesms.recipients.Recipient;

import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import cz.uhk.dip.mmsparams.android.pdus.MmsReadHelper;
import cz.uhk.dip.mmsparams.android.pdus.PduByteHelper;
import cz.uhk.dip.mmsparams.api.enums.ReadStatus;

public class MmsReadReportWSManualJob extends BaseJob
{
    public static final String KEY =  MmsReadReportWSManualJob.class.getSimpleName();

    private static final String TAG = MmsReadReportWSManualJob.class.getSimpleName();

    private static final String KEY_retrieveConf = "retrieveConf";
    private static final String KEY_readStatus = "readStatus";
    private static final String KEY_downloadAfterSec = "downloadAfterSec";
    private static final String KEY_messageId = "messageId";
    private static final String KEY_subscriptionId = "subscriptionId";

    final byte[] retrieveConf;
    final ReadStatus readStatus;
    final int downloadAfterSec;
    final long messageId;
    final int subscriptionId;

    public MmsReadReportWSManualJob(byte[] retrieveConf, ReadStatus readStatus, int downloadAfterSec,
                                    long messageId, int subscriptionId)
    {
        this(new Job.Parameters.Builder()
                        .setQueue("mms-operation")
                        .addConstraint(NetworkConstraint.KEY)
                        .setMaxAttempts(15)
                        .build(),
                retrieveConf, readStatus, downloadAfterSec, messageId, subscriptionId);
    }

    private MmsReadReportWSManualJob(@NonNull Job.Parameters parameters, byte[] retrieveConf, ReadStatus readStatus, int downloadAfterSec,
                                     long messageId, int subscriptionId)
    {
        super(parameters);
        this.retrieveConf = retrieveConf;
        this.readStatus = readStatus;
        this.downloadAfterSec = downloadAfterSec;
        this.messageId = messageId;
        this.subscriptionId = subscriptionId;
    }

    @Override
    protected void onRun() throws Exception
    {
        new Timer().schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                try
                {
                    MyRetrieveConf rc = PduByteHelper.getPdu(retrieveConf);
                    if (rc != null)
                    {
                        int status = PduHeaders.READ_STATUS_READ;
                        if (readStatus != null)
                        {
                            status = readStatus == ReadStatus.DELETED ? PduHeaders.READ_STATUS__DELETED_WITHOUT_BEING_READ : PduHeaders.READ_STATUS_READ;
                        }
                        MmsReadHelper.sendReadReport(context, rc, status, messageId, subscriptionId);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    Log.e(TAG, "sendReadReport", e);
                }
            }
        }, downloadAfterSec * 1000);
    }

    @Override
    protected boolean onShouldRetry(@NonNull Exception e)
    {
        return false;
    }

    @NonNull
    @Override
    public Data serialize()
    {
        return new Data.Builder()

                .putByteArray(KEY_retrieveConf, retrieveConf)
                .putInt(KEY_downloadAfterSec, downloadAfterSec)
                .putLong(KEY_messageId, messageId)
                .putInt(KEY_subscriptionId, subscriptionId)
                .putString(KEY_readStatus, readStatus.toString())
                .build();
    }

    @NonNull
    @Override
    public String getFactoryKey()
    {
        return KEY;
    }

    @Override
    public void onCanceled()
    {
        Log.i(TAG, "onCanceled() messageId: " + messageId);
        DatabaseFactory.getMmsDatabase(context).markAsSentFailed(messageId);
        notifyMediaMessageDeliveryFailed(context, messageId);
    }

    private void notifyMediaMessageDeliveryFailed(Context context, long messageId)
    {
        long threadId = DatabaseFactory.getMmsDatabase(context).getThreadIdForMessage(messageId);
        Recipient recipient = DatabaseFactory.getThreadDatabase(context).getRecipientForThreadId(threadId);

        if (recipient != null)
        {
            MessageNotifier.notifyMessageDeliveryFailed(context, recipient, threadId);
        }
    }

    public static class Factory implements Job.Factory<MmsReadReportWSManualJob>
    {
        @Override
        public @NonNull
        MmsReadReportWSManualJob create(@NonNull Job.Parameters parameters, @NonNull Data data)
        {
            return new MmsReadReportWSManualJob(parameters,
                    data.getByteArray(KEY_retrieveConf),
                    ReadStatus.valueOf(data.getString(KEY_readStatus)),
                    data.getInt(KEY_downloadAfterSec),
                    data.getLong(KEY_messageId),
                    data.getInt(KEY_subscriptionId));
        }
    }
}
