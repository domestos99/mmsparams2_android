package cz.uhk.dip.mmsparams.android.jobs;

import android.app.Activity;
import android.telephony.SmsManager;

import org.thoughtcrime.securesms.ApplicationContext;
import org.thoughtcrime.securesms.database.DatabaseFactory;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.SmsDatabase;
import org.thoughtcrime.securesms.database.model.SmsMessageRecord;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobs.BaseJob;
import org.thoughtcrime.securesms.jobs.SmsSendJob;
import org.thoughtcrime.securesms.jobs.SmsSentJob;
import org.thoughtcrime.securesms.logging.Log;
import org.thoughtcrime.securesms.notifications.MessageNotifier;
import org.thoughtcrime.securesms.service.SmsDeliveryListener;

import androidx.annotation.NonNull;
import cz.uhk.dip.mmsparams.android.websocket.WebSocketErrorFactory;
import cz.uhk.dip.mmsparams.android.websocket.WebSocketUtils;

public class SmsSentWSJob extends BaseJob
{

    public static final String KEY = SmsSentWSJob.class.getSimpleName();

    private static final String TAG = SmsSentWSJob.class.getSimpleName();

    private static final String KEY_MESSAGE_ID = "message_id";
    private static final String KEY_ACTION = "action";
    private static final String KEY_RESULT = "result";
    private static final String KEY_RUN_ATTEMPT = "run_attempt";

    private static final String KEY_messageRequestID = "messageRequestID";
    private static final String KEY_messageSenderKey = "messageSenderKey";
    private static final String KEY_testId = "testId";
    private static final String KEY_pdu = "pdu";
    private static final String KEY_format = "format";

    private long messageId;
    private String action;
    private int result;
    private int runAttempt;

    private final String messageRequestID;
    private final String messageSenderKey;
    private final String testId;
    private final byte[] pdu;
    private final String format;

    public SmsSentWSJob(long messageId, String messageRequestID, String messageSenderKey,
                        String testId, String action, int result, byte[] pdu, String format, int runAttempt)
    {
        this(new Job.Parameters.Builder().build(),
                messageId,
                messageRequestID, messageSenderKey, testId, action, result, pdu, format, runAttempt);
    }

    private SmsSentWSJob(@NonNull Job.Parameters parameters, long messageId, String messageRequestID, String messageSenderKey,
                         String testId, String action, int result, byte[] pdu, String format, int runAttempt)
    {
        super(parameters);

        this.messageId = messageId;
        this.action = action;
        this.result = result;
        this.runAttempt = runAttempt;

        this.messageRequestID = messageRequestID;
        this.messageSenderKey = messageSenderKey;
        this.testId = testId;
        this.pdu = pdu;
        this.format = format;
    }

    @Override
    public @NonNull
    Data serialize()
    {
        return new Data.Builder()
                .putLong(KEY_MESSAGE_ID, messageId)
                .putString(KEY_ACTION, action)
                .putInt(KEY_RESULT, result)
                .putInt(KEY_RUN_ATTEMPT, runAttempt)


                .putString(KEY_messageRequestID, messageRequestID)
                .putString(KEY_messageSenderKey, messageSenderKey)
                .putString(KEY_testId, testId)
                .putByteArray(KEY_pdu, pdu)
                .putString(KEY_format, format)

                .build();
    }

    @Override
    public @NonNull
    String getFactoryKey()
    {
        return KEY;
    }

    @Override
    public void onRun()
    {
        Log.i(TAG, "Got SMS callback: " + action + " , " + result);

        switch (action)
        {
            case SmsDeliveryListener.SENT_SMS_ACTION:
                handleSentResult(messageId, result);
                break;
            case SmsDeliveryListener.DELIVERED_SMS_ACTION:
                handleDeliveredResult(messageId, result);
                break;
        }
    }

    @Override
    public boolean onShouldRetry(@NonNull Exception throwable)
    {
        return false;
    }

    @Override
    public void onCanceled()
    {
    }

    private void handleDeliveredResult(long messageId, int result)
    {
        DatabaseFactory.getSmsDatabase(context).markStatus(messageId, result);

        WebSocketUtils.sendSmsDelivery(messageRequestID, testId, messageSenderKey, pdu, format);
    }

    private void handleSentResult(long messageId, int result)
    {
        try
        {
            SmsDatabase database = DatabaseFactory.getSmsDatabase(context);
            SmsMessageRecord record = database.getMessage(messageId);

            WebSocketUtils.sendSmsResponseOk(result, messageRequestID, messageSenderKey, testId);


            switch (result)
            {
                case Activity.RESULT_OK:
                    database.markAsSent(messageId, false);
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Log.w(TAG, "Service connectivity problem, requeuing...");
                    ApplicationContext.getInstance(context)
                            .getJobManager()
                            .add(new SmsSendJob(context, messageId, record.getIndividualRecipient().getAddress(), runAttempt + 1));
                    break;
                default:
                    database.markAsSentFailed(messageId);
                    MessageNotifier.notifyMessageDeliveryFailed(context, record.getRecipient(), record.getThreadId());
            }
        }
        catch (NoSuchMessageException e)
        {
            Log.w(TAG, e);
        }
    }



    public static final class Factory implements Job.Factory<SmsSentWSJob>
    {
        @Override
        public @NonNull
        SmsSentWSJob create(@NonNull Job.Parameters parameters, @NonNull Data data)
        {
            return new SmsSentWSJob(parameters,
                    data.getLong(KEY_MESSAGE_ID),
                    data.getString(KEY_messageRequestID),
                    data.getString(KEY_messageSenderKey),
                    data.getString(KEY_testId),
                    data.getString(KEY_ACTION),
                    data.getInt(KEY_RESULT),
                    data.getByteArray(KEY_pdu),
                    data.getString(KEY_format),
                    data.getInt(KEY_RUN_ATTEMPT));
        }
    }
}
