package cz.uhk.dip.mmsparams.android.apiinterface;

import android.content.Context;

import org.thoughtcrime.securesms.logging.Log;

import cz.uhk.dip.mmsparams.api.websocket.model.SystemVersionModel;

public class SystemVersionClient extends ApiClientBase
{
    public SystemVersionClient(Context context)
    {
        super(context);
    }

    @Override
    protected String getControllerName()
    {
        return "systemversion";
    }

    public SystemVersionModel getSystemVersion(String address)
    {
        try
        {
            return super.getData(address, SystemVersionModel.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG, "pingServer", e);
            return null;
        }
    }
}
