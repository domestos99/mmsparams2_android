package cz.uhk.dip.mmsparams.android.apiinterface;

import android.content.Context;

import org.thoughtcrime.securesms.logging.Log;

import cz.uhk.dip.mmsparams.api.websocket.model.PingGetModel;

public class PingClient extends ApiClientBase
{
    public PingClient(Context context)
    {
        super(context);
    }

    @Override
    protected String getControllerName()
    {
        return "Ping";
    }

    public boolean pingServer(String address) throws Exception
    {
        try
        {
            PingGetModel pingResult =  super.getData(address, PingGetModel.class);

            if (pingResult == null)
                return false;

            return pingResult.getStatus() == 1;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e(TAG, "pingServer", e);
            throw e;
        }
    }


}
