package com.google.android.mms.pdu_alt;


import com.google.android.mms.InvalidHeaderValueException;

public class MySendConf extends SendConf
{
    public MySendConf() throws InvalidHeaderValueException
    {
        super();
    }

    public MySendConf(PduHeaders headers)
    {
        super(headers);
    }


    public byte[] getContentLocation()
    {
        return mPduHeaders.getTextString(PduHeaders.CONTENT_LOCATION);
    }

    public void setContentLocation(byte[] value)
    {
        mPduHeaders.setTextString(value, PduHeaders.CONTENT_LOCATION);
    }

    public EncodedStringValue getResponseText()
    {
        return mPduHeaders.getEncodedStringValue(PduHeaders.RESPONSE_TEXT);
    }

    public void setResponseText(EncodedStringValue value)
    {
        mPduHeaders.setEncodedStringValue(value, PduHeaders.RESPONSE_TEXT);
    }

    public int getStoreStatus()
    {
        return mPduHeaders.getOctet(PduHeaders.STORE_STATUS);
    }

    public void setStoreStatus(int value) throws InvalidHeaderValueException
    {
        mPduHeaders.setOctet(value, PduHeaders.STORE_STATUS);
    }

    public byte[] getStoreStatusText()
    {
        return mPduHeaders.getTextString(PduHeaders.STORE_STATUS_TEXT);
    }

    public void setStoreStatusText(byte[] value)
    {
        mPduHeaders.setTextString(value, PduHeaders.STORE_STATUS_TEXT);
    }

    /*
     * Optional, not supported header fields:
     *
     *    public byte[] getContentLocation() {return null;}
     *    public void setContentLocation(byte[] value) {}
     *
     *    public EncodedStringValue getResponseText() {return null;}
     *    public void setResponseText(EncodedStringValue value) {}
     *
     *    public byte getStoreStatus() {return 0x00;}
     *    public void setStoreStatus(byte value) {}
     *
     *    public byte[] getStoreStatusText() {return null;}
     *    public void setStoreStatusText(byte[] value) {}
     */
}
