package com.google.android.mms.pdu_alt;

import com.google.android.mms.InvalidHeaderValueException;

public class MyNotificationInd extends NotificationInd
{
    public MyNotificationInd() throws InvalidHeaderValueException
    {
        super();
    }

    MyNotificationInd(PduHeaders headers)
    {
        super(headers);
    }


    public byte[] getApplicId()
    {
        return mPduHeaders.getTextString(PduHeaders.APPLIC_ID);
    }

    public byte[] getAuxApplicId()
    {
        return mPduHeaders.getTextString(PduHeaders.AUX_APPLIC_ID);
    }

    public int getDrmContent()
    {
        return mPduHeaders.getOctet(PduHeaders.DRM_CONTENT);
    }

    public int getDistributionIndicator()
    {
        return mPduHeaders.getOctet(PduHeaders.DISTRIBUTION_INDICATOR);
    }

//    public ElementDescriptorValue getElementDescriptor()
//    {
//        return null;
//    }

    public int getPriority()
    {
        return mPduHeaders.getOctet(PduHeaders.PRIORITY);
    }

    public int getRecommendedRetrievalMode()
    {
        return mPduHeaders.getOctet(PduHeaders.RECOMMENDED_RETRIEVAL_MODE);
    }

    public int getRecommendedRetrievalModeText()
    {
        return mPduHeaders.getOctet(PduHeaders.RECOMMENDED_RETRIEVAL_MODE_TEXT);
    }

    public byte[] getReplaceId()
    {
        return mPduHeaders.getTextString(PduHeaders.REPLACE_ID);
    }

    public byte[] getReplyApplicId()
    {
        return mPduHeaders.getTextString(PduHeaders.REPLY_APPLIC_ID);
    }

    public int getReplyCharging()
    {
        return mPduHeaders.getOctet(PduHeaders.REPLY_CHARGING);
    }

    public int getReplyChargingDeadline()
    {
        return mPduHeaders.getOctet(PduHeaders.REPLY_CHARGING_DEADLINE);
    }

    public byte[] getReplyChargingId()
    {
        return mPduHeaders.getTextString(PduHeaders.REPLY_CHARGING_ID);
    }

    public long getReplyChargingSize()
    {
        return mPduHeaders.getLongInteger(PduHeaders.REPLY_CHARGING_SIZE);
    }

    public int getStored()
    {
        return mPduHeaders.getOctet(PduHeaders.STORED);
    }



       /*
     * Optional, not supported header fields:
     *
     *     public byte[] getApplicId() {return null;}
     *     public void setApplicId(byte[] value) {}
     *
     *     public byte[] getAuxApplicId() {return null;}
     *     public void getAuxApplicId(byte[] value) {}
     *
     *     public byte getDrmContent() {return 0x00;}
     *     public void setDrmContent(byte value) {}
     *
     *     public byte getDistributionIndicator() {return 0x00;}
     *     public void setDistributionIndicator(byte value) {}
     *
     *     public ElementDescriptorValue getElementDescriptor() {return null;}
     *     public void getElementDescriptor(ElementDescriptorValue value) {}
     *
     *     public byte getPriority() {return 0x00;}
     *     public void setPriority(byte value) {}
     *
     *     public byte getRecommendedRetrievalMode() {return 0x00;}
     *     public void setRecommendedRetrievalMode(byte value) {}
     *
     *     public byte getRecommendedRetrievalModeText() {return 0x00;}
     *     public void setRecommendedRetrievalModeText(byte value) {}
     *
     *     public byte[] getReplaceId() {return 0x00;}
     *     public void setReplaceId(byte[] value) {}
     *
     *     public byte[] getReplyApplicId() {return 0x00;}
     *     public void setReplyApplicId(byte[] value) {}
     *
     *     public byte getReplyCharging() {return 0x00;}
     *     public void setReplyCharging(byte value) {}
     *
     *     public byte getReplyChargingDeadline() {return 0x00;}
     *     public void setReplyChargingDeadline(byte value) {}
     *
     *     public byte[] getReplyChargingId() {return 0x00;}
     *     public void setReplyChargingId(byte[] value) {}
     *
     *     public long getReplyChargingSize() {return 0;}
     *     public void setReplyChargingSize(long value) {}
     *
     *     public byte getStored() {return 0x00;}
     *     public void setStored(byte value) {}
     */


}
