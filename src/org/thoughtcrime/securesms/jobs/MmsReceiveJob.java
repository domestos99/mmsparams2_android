package org.thoughtcrime.securesms.jobs;

import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.logging.Log;

import androidx.annotation.NonNull;
import cz.uhk.dip.mmsparams.android.db.MessagePduDatabase;
import cz.uhk.dip.mmsparams.android.jobs.MmsDownloadWSManualJob;
import cz.uhk.dip.mmsparams.android.log.DBLog;
import cz.uhk.dip.mmsparams.android.log.MmsErrorLogger;
import cz.uhk.dip.mmsparams.android.mms.MessageThreadIDPair;
import cz.uhk.dip.mmsparams.android.pdus.NotifyRespIndHelper;
import cz.uhk.dip.mmsparams.android.pdus.PduByteHelper;
import cz.uhk.dip.mmsparams.android.preferences.MmsParamsPreferences;
import cz.uhk.dip.mmsparams.android.websocket.WebSocketPduHelper;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsDownloadOptionsModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsRecipientPhoneProfile;

import android.util.Pair;

import com.google.android.mms.pdu_alt.GenericPdu;
import com.google.android.mms.pdu_alt.MyDeliveryInd;
import com.google.android.mms.pdu_alt.MyNotificationInd;
import com.google.android.mms.pdu_alt.MyReadOrigInd;
import com.google.android.mms.pdu_alt.NotificationInd;
import com.google.android.mms.pdu_alt.PduHeaders;
import com.google.android.mms.pdu_alt.MyPduParser;

import org.thoughtcrime.securesms.ApplicationContext;
import org.thoughtcrime.securesms.database.Address;
import org.thoughtcrime.securesms.database.DatabaseFactory;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;

import java.io.IOException;

public class MmsReceiveJob extends BaseJob
{

    public static final String KEY = "MmsReceiveJob";

    private static final String TAG = MmsReceiveJob.class.getSimpleName();

    private static final String KEY_DATA = "data";
    private static final String KEY_SUBSCRIPTION_ID = "subscription_id";

    private byte[] data;
    private int subscriptionId;

    public MmsReceiveJob(byte[] data, int subscriptionId)
    {
        this(new Job.Parameters.Builder().setMaxAttempts(25).build(), data, subscriptionId);
    }

    private MmsReceiveJob(@NonNull Job.Parameters parameters, byte[] data, int subscriptionId)
    {
        super(parameters);

        this.data = data;
        this.subscriptionId = subscriptionId;
    }

    @Override
    public @NonNull
    Data serialize()
    {
        return new Data.Builder().putString(KEY_DATA, Base64.encodeBytes(data))
                .putInt(KEY_SUBSCRIPTION_ID, subscriptionId)
                .build();
    }

    @Override
    public @NonNull
    String getFactoryKey()
    {
        return KEY;
    }

    @Override
    public void onRun()
    {
        if (data == null)
        {
            Log.w(TAG, "Received NULL pdu, ignoring...");
            return;
        }

        GenericPdu pdu = null;

        try
        {
            pdu = PduByteHelper.getPdu(data);
        }
        catch (RuntimeException e)
        {
            Log.w(TAG, e);
        }

        if (isNotification(pdu) && !isBlocked(pdu))
        {
            MmsDatabase database = DatabaseFactory.getMmsDatabase(context);
            MessageThreadIDPair messageAndThreadId = database.insertMessageInbox((NotificationInd) pdu, subscriptionId);


            final MyNotificationInd pduNotificationInd = (MyNotificationInd) pdu;
            PduByteHelper.storeMessagePdu(context, messageAndThreadId.first, PduHeaders.MESSAGE_TYPE_NOTIFICATION_IND, data);
            WebSocketPduHelper.handleSendNotificationInd(pduNotificationInd);


            Log.i(TAG, "Inserted received MMS notification...");

            boolean downloadNow = MmsParamsPreferences.isAutoDownloadMms(context);


            MmsRecipientPhoneProfile recipientProfile = MmsParamsPreferences.getMmsRecipientPhoneProfile(context);


            if (recipientProfile != null)
            {
                MmsDownloadOptionsModel downloadOptions = recipientProfile.getMmsDownloadOptionsModel();

                if (downloadOptions != null)
                {
                    downloadNow = downloadOptions.isAutoDownload();
                }

                if (downloadOptions != null && !downloadOptions.isAutoDownload())
                {
                    int downloadAfterSeconds = downloadOptions.getDownloadAfterSeconds();

                    // If not download now, send status Deferred
                    if (!downloadNow)
                    {
                        sendNotifyRespIndDeferred(pduNotificationInd.getTransactionId(), messageAndThreadId.getMessageId());
                    }

                    if (downloadAfterSeconds < 0)
                        return;

                    // DownloadLater
                    ApplicationContext.getInstance(context)
                            .getJobManager()
                            .add(new MmsDownloadWSManualJob(
                                    messageAndThreadId.getMessageId(),
                                    messageAndThreadId.getThreadID(),
                                    true, downloadAfterSeconds));

                    return;
                }
            }

            // Send NotifyRespInd

            // If not download now, send status Deferred
            if (!downloadNow)
            {
                sendNotifyRespIndDeferred(pduNotificationInd.getTransactionId(), messageAndThreadId.getMessageId());
            }

            ApplicationContext.getInstance(context)
                    .getJobManager()
                    .add(new MmsDownloadJob(
                            messageAndThreadId.getMessageId(),
                            messageAndThreadId.getThreadID(),
                            true, downloadNow, false));
        }
        else if (isDeliveryInd(pdu))
        {
            // Save Delivery Report
            MyDeliveryInd deliveryInd = (MyDeliveryInd) pdu;

// ...
            MessagePduDatabase messagePduDatabase = DatabaseFactory.getmessagePduDatabase(context);
            long msgID = messagePduDatabase.getbyMsgPduID(deliveryInd.getMessageId());

            PduByteHelper.storeMessagePdu(context, msgID, PduHeaders.MESSAGE_TYPE_DELIVERY_IND, data);
            WebSocketPduHelper.handleSendDeliveryInd(deliveryInd);
        }
        else if (isReadOrigInd(pdu))
        {
            MyReadOrigInd readOrigInd = (MyReadOrigInd) pdu;


            MessagePduDatabase messagePduDatabase = DatabaseFactory.getmessagePduDatabase(context);

            long msgID = messagePduDatabase.getbyMsgPduID(readOrigInd.getMessageId());

            PduByteHelper.storeMessagePdu(context, msgID, PduHeaders.MESSAGE_TYPE_READ_ORIG_IND, data);
            WebSocketPduHelper.handleSendReadOrigInd(readOrigInd);


        }
        else if (isNotification(pdu))
        {
            Log.w(TAG, "*** Received blocked MMS, ignoring...");
        }
    }

    @Override
    public void onCanceled()
    {
        // TODO
    }

    @Override
    public boolean onShouldRetry(@NonNull Exception exception)
    {
        return false;
    }

    private boolean isBlocked(GenericPdu pdu)
    {
        if (pdu.getFrom() != null && pdu.getFrom().getTextString() != null)
        {
            Recipient recipients = Recipient.from(context, Address.fromExternal(context, Util.toIsoString(pdu.getFrom().getTextString())), false);
            return recipients.isBlocked();
        }

        return false;
    }

    private boolean isNotification(GenericPdu pdu)
    {
        return pdu != null && pdu.getMessageType() == PduHeaders.MESSAGE_TYPE_NOTIFICATION_IND;
    }

    private boolean isDeliveryInd(GenericPdu pdu)
    {
        return pdu != null && pdu.getMessageType() == PduHeaders.MESSAGE_TYPE_DELIVERY_IND;
    }

    private boolean isReadOrigInd(GenericPdu pdu)
    {
        return pdu != null && pdu.getMessageType() == PduHeaders.MESSAGE_TYPE_READ_ORIG_IND;
    }


    private void sendNotifyRespIndDeferred(byte[] transactionId, long messageID)
    {
        try
        {
            NotifyRespIndHelper.sendNotifyRespIndRetrieved(context, transactionId, messageID, subscriptionId, NotifyRespIndHelper.RetrievedStatus.DEFERRED);
        }
        catch (Exception ex)
        {
            DBLog.logToDB(context, "Send NotifyRespInd Deferred", TAG, ex);


            MmsErrorLogger mmsErrorLogger = new MmsErrorLogger(context);
            mmsErrorLogger.setMessageID(messageID);
            mmsErrorLogger.log(messageID, TAG, "sendNotifyRespIndDeferred", ex);
        }
    }


    public static final class Factory implements Job.Factory<MmsReceiveJob>
    {
        @Override
        public @NonNull
        MmsReceiveJob create(@NonNull Parameters parameters, @NonNull Data data)
        {
            try
            {
                return new MmsReceiveJob(parameters, Base64.decode(data.getString(KEY_DATA)), data.getInt(KEY_SUBSCRIPTION_ID));
            }
            catch (IOException e)
            {
                throw new AssertionError(e);
            }
        }
    }
}
