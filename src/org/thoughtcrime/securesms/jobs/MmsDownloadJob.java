package org.thoughtcrime.securesms.jobs;

import android.net.Uri;

import com.google.android.mms.pdu_alt.CharacterSets;
import com.google.android.mms.pdu_alt.EncodedStringValue;
import com.google.android.mms.pdu_alt.MyRetrieveConf;
import com.google.android.mms.pdu_alt.PduBody;
import com.google.android.mms.pdu_alt.PduHeaders;
import com.google.android.mms.pdu_alt.PduPart;

import org.thoughtcrime.securesms.ApplicationContext;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.UriAttachment;
import org.thoughtcrime.securesms.database.Address;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.DatabaseFactory;
import org.thoughtcrime.securesms.database.MessagingDatabase.InsertResult;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.logging.Log;
import org.thoughtcrime.securesms.mms.ApnUnavailableException;
import org.thoughtcrime.securesms.mms.CompatMmsConnection;
import org.thoughtcrime.securesms.mms.IncomingMediaMessage;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.MmsRadioException;
import org.thoughtcrime.securesms.mms.PartParser;
import org.thoughtcrime.securesms.notifications.MessageNotifier;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.libsignal.DuplicateMessageException;
import org.whispersystems.libsignal.InvalidMessageException;
import org.whispersystems.libsignal.LegacyMessageException;
import org.whispersystems.libsignal.NoSessionException;
import org.whispersystems.libsignal.util.guava.Optional;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import cz.uhk.dip.mmsparams.android.db.MessagePduDatabase;
import cz.uhk.dip.mmsparams.android.db.MmsErrorDatabase;
import cz.uhk.dip.mmsparams.android.jobs.MmsReadReportWSManualJob;
import cz.uhk.dip.mmsparams.android.log.DBLog;
import cz.uhk.dip.mmsparams.android.log.MmsErrorLogger;
import cz.uhk.dip.mmsparams.android.pdus.AcknowledgeIndHelper;
import cz.uhk.dip.mmsparams.android.pdus.NotifyRespIndHelper;
import cz.uhk.dip.mmsparams.android.pdus.PduByteHelper;
import cz.uhk.dip.mmsparams.android.preferences.MmsParamsPreferences;
import cz.uhk.dip.mmsparams.android.websocket.WebSocketPduHelper;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsReadReportOptionsModel;
import cz.uhk.dip.mmsparams.api.websocket.model.mms.MmsRecipientPhoneProfile;

public class MmsDownloadJob extends BaseJob
{

    public static final String KEY = "MmsDownloadJob";

    private static final String TAG = MmsDownloadJob.class.getSimpleName();

    private static final String KEY_MESSAGE_ID = "message_id";
    private static final String KEY_THREAD_ID = "thread_id";
    private static final String KEY_AUTOMATIC = "automatic";
    private static final String KEY_downloadNow = "downloadNow";
    private static final String KEY_isManualCall = "isManualCall";

    private final long messageId;
    private final long threadId;
    private final boolean automatic;
    private final boolean downloadNow;
    private final boolean isManualCall;

    public MmsDownloadJob(long messageId, long threadId, boolean automatic, boolean downloadNow, boolean isManualCall)
    {
        this(new Job.Parameters.Builder()
                        .setQueue("mms-operation")
                        .setMaxAttempts(25)
                        .build(),
                messageId,
                threadId,
                automatic,
                downloadNow,
                isManualCall);

    }

    private MmsDownloadJob(@NonNull Job.Parameters parameters, long messageId, long threadId, boolean automatic, boolean downloadNow, boolean isManualCall)
    {
        super(parameters);

        this.messageId = messageId;
        this.threadId = threadId;
        this.automatic = automatic;
        this.downloadNow = downloadNow;
        this.isManualCall = isManualCall;
    }

    @Override
    public @NonNull
    Data serialize()
    {
        return new Data.Builder()
                .putLong(KEY_MESSAGE_ID, messageId)
                .putLong(KEY_THREAD_ID, threadId)
                .putBoolean(KEY_AUTOMATIC, automatic)
                .putBoolean(KEY_downloadNow, downloadNow)
                .putBoolean(KEY_isManualCall, isManualCall)
                .build();
    }

    @Override
    public @NonNull
    String getFactoryKey()
    {
        return KEY;
    }

    @Override
    public void onAdded()
    {
        if (automatic && KeyCachingService.isLocked(context))
        {
            DatabaseFactory.getMmsDatabase(context).markIncomingNotificationReceived(threadId);
            MessageNotifier.updateNotification(context);
        }
    }

    @Override
    public void onRun()
    {
        MmsErrorLogger mmsErrorLogger = new MmsErrorLogger(context);

        MmsDatabase database = DatabaseFactory.getMmsDatabase(context);

        if (!downloadNow)
        {
            MessageNotifier.updateNotification(context, threadId);
            return;
        }


        mmsErrorLogger.setMessageID(messageId);
        Optional<MmsDatabase.MmsNotificationInfo> notification = database.getNotification(messageId);

        if (!notification.isPresent())
        {
            Log.w(TAG, "No notification for ID: " + messageId);
            return;
        }

        try
        {
            if (notification.get().getContentLocation() == null)
            {
                throw new MmsException("Notification content location was null.");
            }

//            if (!TextSecurePreferences.isPushRegistered(context))
//            {
//                throw new MmsException("Not registered");
//            }

            database.markDownloadState(messageId, MmsDatabase.Status.DOWNLOAD_CONNECTING);

            String contentLocation = notification.get().getContentLocation();
            byte[] transactionId = new byte[0];

            try
            {
                if (notification.get().getTransactionId() != null)
                {
                    transactionId = notification.get().getTransactionId().getBytes(CharacterSets.MIMENAME_ISO_8859_1);
                }
                else
                {
                    Log.w(TAG, "No transaction ID!");
                }
            }
            catch (UnsupportedEncodingException e)
            {
                Log.w(TAG, e);
            }

            Log.w(TAG, "Downloading mms at " + Uri.parse(contentLocation).getHost() + ", subscription ID: " + notification.get().getSubscriptionId());

            mmsErrorLogger.setMessageID(messageId);
            byte[] retrieveBytes = new CompatMmsConnection(context, mmsErrorLogger).retrieve(contentLocation, transactionId, messageId, notification.get().getSubscriptionId());

            if (retrieveBytes == null)
            {
                throw new MmsException("RetrieveConf was null");
            }


            mmsErrorLogger.setMessageID(messageId);
            long newMsgID = storeRetrievedMms(contentLocation, messageId, threadId, retrieveBytes, notification.get().getSubscriptionId(), notification.get().getFrom());
            mmsErrorLogger.setMessageID(newMsgID);

            if (newMsgID == -2)
            {
                return;
            }

            // Send NotifyRespInd of Acknowledgement
            if (isManualCall)
            {
                // User ordered to download
                // Acknowlege
                sendAcknowledge(transactionId, newMsgID, notification.get().getSubscriptionId());
            }
            else
            {
                // System downloading after receive
                // NotifyRespInd
                sendNotifyRespIndRetrieved(transactionId, newMsgID, notification.get().getSubscriptionId());
            }

            // Handle read report
            MmsRecipientPhoneProfile recipientProfile = MmsParamsPreferences.getMmsRecipientPhoneProfile(context);
            if (recipientProfile != null)
            {
                MmsReadReportOptionsModel readReportOptions = recipientProfile.getMmsReadReportOptionsModel();

                if (readReportOptions == null)
                    return;

                if (!readReportOptions.isSendReadReport())
                    return;

                int sendRRAfterSeconds = readReportOptions.getSendAfterSeconds();
                if (sendRRAfterSeconds < 0)
                    return;

                // DownloadLater
                ApplicationContext.getInstance(context)
                        .getJobManager()
                        .add(new MmsReadReportWSManualJob(
                                retrieveBytes,
                                readReportOptions.getReadStatus(),
                                readReportOptions.getSendAfterSeconds(),
                                messageId,
                                notification.get().getSubscriptionId()));

                return;
            }


//            long msgID = getIntent().getLongExtra(MESSAGE_ID_EXTRA, -1);
//            MmsReceiveHandler receiveHandler = new MmsReceiveHandler(context);
//            receiveHandler.send(retrieveBytes);

        }
        catch (ApnUnavailableException e)
        {
            Log.w(TAG, e);
            handleDownloadError(messageId, threadId, MmsDatabase.Status.DOWNLOAD_APN_UNAVAILABLE,
                    automatic);

            mmsErrorLogger.log(messageId, TAG, "DOWNLOAD_APN_UNAVAILABLE", e);
        }
        catch (MmsException e)
        {
            Log.w(TAG, e);
            handleDownloadError(messageId, threadId,
                    MmsDatabase.Status.DOWNLOAD_HARD_FAILURE,
                    automatic);

            mmsErrorLogger.log(messageId, TAG, "DOWNLOAD_HARD_FAILURE", e);
        }
        catch (MmsRadioException | IOException e)
        {
            Log.w(TAG, e);
            handleDownloadError(messageId, threadId,
                    MmsDatabase.Status.DOWNLOAD_SOFT_FAILURE,
                    automatic);

            mmsErrorLogger.log(messageId, TAG, "DOWNLOAD_SOFT_FAILURE", e);
        }
        catch (DuplicateMessageException e)
        {
            Log.w(TAG, e);
            database.markAsDecryptDuplicate(messageId, threadId);


            mmsErrorLogger.log(messageId, TAG, e);
        }
        catch (LegacyMessageException e)
        {
            Log.w(TAG, e);
            database.markAsLegacyVersion(messageId, threadId);
            mmsErrorLogger.log(messageId, TAG, e);
        }
        catch (NoSessionException e)
        {
            Log.w(TAG, e);
            database.markAsNoSession(messageId, threadId);
            mmsErrorLogger.log(messageId, TAG, e);
        }
        catch (InvalidMessageException e)
        {
            Log.w(TAG, e);
            database.markAsDecryptFailed(messageId, threadId);
            mmsErrorLogger.log(messageId, TAG, e);
        }
    }


    private void sendAcknowledge(byte[] transactionId,
                                 long messageID, int subscriptionId)
            throws ApnUnavailableException
    {
        try
        {
            AcknowledgeIndHelper.sendAcknowledge(context, transactionId, messageID, subscriptionId);
        }
        catch (Exception ex)
        {
            DBLog.logToDB(context, "Send Acknowledge", TAG, ex);

            MmsErrorLogger mmsErrorLogger = new MmsErrorLogger(context);
            mmsErrorLogger.setMessageID(messageID);
            mmsErrorLogger.log(messageId, TAG, "sendAcknowledge", ex);
        }
    }

    private void sendNotifyRespIndRetrieved(byte[] transactionId, long messageID, int subscriptionId)
    {
        try
        {
            NotifyRespIndHelper.sendNotifyRespIndRetrieved(context, transactionId, messageID, subscriptionId, NotifyRespIndHelper.RetrievedStatus.RETRIEVED);
        }
        catch (Exception ex)
        {
            DBLog.logToDB(context, "Send NotifyRespInd Retrieved", TAG, ex);

            MmsErrorLogger mmsErrorLogger = new MmsErrorLogger(context);
            mmsErrorLogger.setMessageID(messageID);
            mmsErrorLogger.log(messageId, TAG, "sendNotifyRespIndRetrieved", ex);
        }
    }


    @Override
    public void onCanceled()
    {
        MmsDatabase database = DatabaseFactory.getMmsDatabase(context);
        database.markDownloadState(messageId, MmsDatabase.Status.DOWNLOAD_SOFT_FAILURE);

        if (automatic)
        {
            database.markIncomingNotificationReceived(threadId);
            MessageNotifier.updateNotification(context, threadId);
        }
    }

    @Override
    public boolean onShouldRetry(@NonNull Exception exception)
    {
        return false;
    }

    private long storeRetrievedMms(final String contentLocation,
                                   final long messageId, final long threadId, final byte[] retrievedByte,
                                   final int subscriptionId, @Nullable Address notificationFrom)
            throws MmsException, NoSessionException, DuplicateMessageException, InvalidMessageException,
            LegacyMessageException
    {
        MyRetrieveConf retrieved = (MyRetrieveConf) PduByteHelper.getPdu(retrievedByte); // new MyPduParser(retrievedByte).parse());


        if (retrieved == null)
        {
            DBLog.logToDB(context, "storeRetrievedMms: retrieved is null", TAG, null);
            return -2;
        }

        MmsDatabase database = DatabaseFactory.getMmsDatabase(context);
        Optional<Address> group = Optional.absent();
        Set<Address> members = new HashSet<>();
        String body = null;
        List<Attachment> attachments = new LinkedList<>();

        Address from;

        if (retrieved.getFrom() != null)
        {
            from = Address.fromExternal(context, Util.toIsoString(retrieved.getFrom().getTextString()));
        }
        else if (notificationFrom != null)
        {
            from = notificationFrom;
        }
        else
        {
            from = Address.UNKNOWN;
        }

        if (retrieved.getTo() != null)
        {
            for (EncodedStringValue toValue : retrieved.getTo())
            {
                members.add(Address.fromExternal(context, Util.toIsoString(toValue.getTextString())));
            }
        }

        if (retrieved.getCc() != null)
        {
            for (EncodedStringValue ccValue : retrieved.getCc())
            {
                members.add(Address.fromExternal(context, Util.toIsoString(ccValue.getTextString())));
            }
        }

        members.add(from);
        members.add(Address.fromExternal(context, TextSecurePreferences.getLocalNumber(context)));

        if (retrieved.getBody() != null)
        {
            body = PartParser.getMessageText(retrieved.getBody());
            PduBody media = PartParser.getSupportedMediaParts(retrieved.getBody());

            for (int i = 0; i < media.getPartsNum(); i++)
            {
                PduPart part = media.getPart(i);

                if (part.getData() != null)
                {
                    Uri uri = BlobProvider.getInstance().forData(part.getData()).createForSingleUseInMemory();
                    String name = null;

                    if (part.getName() != null) name = Util.toIsoString(part.getName());

                    attachments.add(new UriAttachment(uri, Util.toIsoString(part.getContentType()),
                            AttachmentDatabase.TRANSFER_PROGRESS_DONE,
                            part.getData().length, name, false, false, null, null));
                }
            }
        }

        if (members.size() > 2)
        {
            group = Optional.of(Address.fromSerialized(DatabaseFactory.getGroupDatabase(context).getOrCreateGroupForMembers(new LinkedList<>(members), true)));
        }

        IncomingMediaMessage message = new IncomingMediaMessage(from, group, body, retrieved.getDate() * 1000L, attachments, subscriptionId, 0, false, false, false);
        Optional<InsertResult> insertResult = database.insertMessageInbox(message, contentLocation, threadId);

        PduByteHelper.storeMessagePdu(context, messageId, PduHeaders.MESSAGE_TYPE_RETRIEVE_CONF, retrievedByte);
        WebSocketPduHelper.handleRetrieveConf(retrieved);


        if (insertResult.isPresent())
        {
            MessagePduDatabase messagePduDatabase = DatabaseFactory.getmessagePduDatabase(context);
            messagePduDatabase.changeMessageID(messageId, insertResult.get().getMessageId());

            MmsErrorDatabase mmsErrorDatabase = DatabaseFactory.getMmsErrorDatabase(context);
            mmsErrorDatabase.changeMessageID(messageId, insertResult.get().getMessageId());

            database.delete(messageId);


            MessageNotifier.updateNotification(context, insertResult.get().getThreadId());

            return insertResult.get().getMessageId();
        }

        return -1;
    }

    private void handleDownloadError(long messageId, long threadId, int downloadStatus, boolean automatic)
    {
        MmsDatabase db = DatabaseFactory.getMmsDatabase(context);

        db.markDownloadState(messageId, downloadStatus);

        if (automatic)
        {
            db.markIncomingNotificationReceived(threadId);
            MessageNotifier.updateNotification(context, threadId);
        }
    }

    public static final class Factory implements Job.Factory<MmsDownloadJob>
    {
        @Override
        public @NonNull
        MmsDownloadJob create(@NonNull Parameters parameters, @NonNull Data data)
        {
            return new MmsDownloadJob(parameters,
                    data.getLong(KEY_MESSAGE_ID),
                    data.getLong(KEY_THREAD_ID),
                    data.getBoolean(KEY_AUTOMATIC),
                    data.getBoolean(KEY_downloadNow),
                    data.getBoolean(KEY_isManualCall));
        }
    }
}
