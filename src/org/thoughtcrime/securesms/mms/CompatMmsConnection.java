package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import cz.uhk.dip.mmsparams.android.exception.EmptyException;
import cz.uhk.dip.mmsparams.android.log.MmsErrorLogger;
import cz.uhk.dip.mmsparams.android.pdus.PduByteHelper;

import org.thoughtcrime.securesms.logging.Log;

import com.google.android.mms.pdu_alt.GenericPdu;
import com.google.android.mms.pdu_alt.MySendConf;
import com.google.android.mms.pdu_alt.PduHeaders;
import com.google.android.mms.pdu_alt.RetrieveConf;
import com.google.android.mms.pdu_alt.SendConf;

import org.thoughtcrime.securesms.transport.UndeliverableMessageException;

import java.io.IOException;

public class CompatMmsConnection implements OutgoingMmsConnection, IncomingMmsConnection
{
    private static final String TAG = CompatMmsConnection.class.getSimpleName();

    private Context context;
    private final MmsErrorLogger mmsErrorLogger;

    public CompatMmsConnection(Context context, final MmsErrorLogger mmsErrorLogger)
    {
        this.context = context;
        this.mmsErrorLogger = mmsErrorLogger;
    }

    @Nullable
    @Override
    public byte[] send(@NonNull byte[] pduBytes, int subscriptionId)
            throws UndeliverableMessageException
    {
        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP_MR1)
        {
            try
            {
                Log.i(TAG, "Sending via Lollipop API");
                return new OutgoingLollipopMmsConnection(context).send(pduBytes, subscriptionId);
            }
            catch (UndeliverableMessageException e)
            {
                Log.w(TAG, e);
                mmsErrorLogger.log(TAG, e);
            }

            Log.i(TAG, "Falling back to legacy connection...");
        }

        if (subscriptionId == -1)
        {
            Log.i(TAG, "Sending via legacy connection");
            try
            {
                byte[] result = new OutgoingLegacyMmsConnection(context).send(pduBytes, subscriptionId);
                GenericPdu pduResult = PduByteHelper.getPdu(result); // new MyPduParser(result).parse();

                if (pduResult == null || pduResult.getMessageType() != PduHeaders.MESSAGE_TYPE_SEND_CONF)
                {
                    return null;
                }

                MySendConf resultObj = (MySendConf) pduResult;
                if (result != null && resultObj.getResponseStatus() == PduHeaders.RESPONSE_STATUS_OK)
                {
                    return result;
                }
                else
                {
                    String msg = "Got bad legacy response: " + (result != null ? resultObj.getResponseStatus() : null);
                    Log.w(TAG, msg);
                    mmsErrorLogger.log(TAG, new EmptyException(msg));
                }
            }
            catch (UndeliverableMessageException | ApnUnavailableException e)
            {
                Log.w(TAG, e);
                mmsErrorLogger.log(TAG, e);
            }
        }

        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP && VERSION.SDK_INT < VERSION_CODES.LOLLIPOP_MR1)
        {
            Log.i(TAG, "Falling back to sending via Lollipop API");
            return new OutgoingLollipopMmsConnection(context).send(pduBytes, subscriptionId);
        }

        throw new UndeliverableMessageException("Both lollipop and legacy connections failed...");
    }

    @Nullable
    @Override
    public byte[] retrieve(@NonNull String contentLocation,
                           byte[] transactionId,
                           long messageID,
                           int subscriptionId)
            throws MmsException, MmsRadioException, ApnUnavailableException, IOException
    {
        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP_MR1)
        {
            Log.i(TAG, "Receiving via Lollipop API");
            try
            {
                return new IncomingLollipopMmsConnection(context).retrieve(contentLocation, transactionId, messageID, subscriptionId);
            }
            catch (MmsException e)
            {
                Log.w(TAG, e);
            }

            Log.i(TAG, "Falling back to receiving via legacy connection");
        }

        if (VERSION.SDK_INT < 22 || subscriptionId == -1)
        {
            Log.i(TAG, "Receiving via legacy API");
            try
            {
                return new IncomingLegacyMmsConnection(context).retrieve(contentLocation, transactionId, messageID, subscriptionId);
            }
            catch (MmsRadioException | ApnUnavailableException | IOException e)
            {
                Log.w(TAG, e);
            }
        }

        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP && VERSION.SDK_INT < VERSION_CODES.LOLLIPOP_MR1)
        {
            Log.i(TAG, "Falling back to receiving via Lollipop API");
            return new IncomingLollipopMmsConnection(context).retrieve(contentLocation, transactionId, messageID, subscriptionId);
        }

        throw new IOException("Both lollipop and fallback APIs failed...");
    }
}
